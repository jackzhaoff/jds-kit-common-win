///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSStoreAPI.h
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        Define the C++ JDS Store API
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#ifdef __cplusplus

#include <JDSKit/JDSKit.hpp>

#include <memory>
#include <string>
#include <map>

namespace jds
{
    /*!
     * Describe how a store was loaded
     */
    enum LoadType
    {
        /*! A full load */
        FULL_LOAD,
        
        /*! An incremental load */
        INCREMENTAL_LOAD
    };
    
    typedef enum LoadType LoadType;

    /*!
     * Validate a given store against a provied schema.
     *
     * @param schema The JDS Schema instance to validate against.
     * @param storeDescriptor The descriptor for the store to validate
     * @param scope The complexity of the validation.
     * @return The state of the validation.
     * @throws JDSException if an error occurred during validation.
     */
    StoreState validateStore(const Schema&              schema,
                             const StoreConfiguration&  storeDescriptor,
                             const ValidationScope      scope = ValidationScope::FAST);
    
    /*!
     * Validate all provided stores against a given schema.
     *
     * @param schema The JDS Schema instance to validate against.
     * @param storeConfigs The descriptor for the stores to validate
     * @param scope The complexity of the validation.
     * @return The state of the validation.
     * @throws JDSException if an error occurred during validation.
     */
    StoreState validateStores(const Schema&                 schema,
                              const StoreConfigurations&    storeConfigs,
                              const ValidationScope         scope = ValidationScope::FAST);
    
    /*!
     * Validate the single provided store without checking against
     * a particular schema.
     *
     * @param storeDescriptor The descriptor for the store to validate
     * @return The state of the validation.
     * @throws JDSException if an error occurred during validation.
     */
    StoreState validateStore(const StoreConfiguration& storeDescriptor);

    /*!
     * Validate the database schema of a given database connection without
     * checking against a particular schema.
     *
     * @param db The database connection to validate
     * @return The state of the validation.
     * @throws JDSException if an error occurred during validation.
     */
    StoreState validateStore(DBConnection& db);

    /*!
     * Read the effectivity information stored in a given store
     *
     * @param storeConfiguration The store to read the effectivity data from
     * @return The effectivity data loaded from that store.
     * @throws JDSException if an error occurred loading the data from the store.
     */
    EffectivityInfo getEffectivityFromStore(const StoreConfiguration& storeConfiguration);

    /*!
     * Read the version information stored in a given store
     *
     * @param storeConfiguration The store to read the effectivity data from
     * @return The version data loaded from that store.
     * @throws JDSException if an error occurred loading the data from the store.
     */
    VersionInfo getVersionFromStore(const StoreConfiguration& storeConfiguration);

    /*!
     * Load all loaded parts from the store and return them as a list of strings
     *
     * @param storeConfiguration The store to read the effectivity data from
     * @return The list with part names. The list will be empty if no parts are loaded.
     * @throws JDSException if an error occurred loading the data from the store.
     */
    std::vector<std::string> getPartsFromStore(const StoreConfiguration& storeConfiguration);

    /*!
     * Load the serial number from the store and return it as a string
     *
     * @param storeConfiguration The store to read the effectivity data from
     * @return The serial number found in the store. The string will be empty if no
     *         serial number could be loaded.
     * @throws JDSException if an error occurred loading the data from the store.
     */
    std::string getSerialFromStore(const StoreConfiguration& storeConfiguration);

    /*!
     * Retrieve information if the given store was loaded from a full or a
     * delta loadset.
     *
     * @param storeConfiguration The store to retrieve the load type from
     * @return Type of the load that was used for loading this store. Can either
     *         be FULL_LOAD or INCREMENTAL_LOAD.
     * @throws JDSException if an error occurred loading the data from the store.
     */
    LoadType getLoadTypeFromStore(const StoreConfiguration& storeConfiguration);
}

#endif
