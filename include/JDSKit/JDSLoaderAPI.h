///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSLoaderAPI.h
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        JDS Loader API
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#import <JDSKit/JDSKit.h>

@class JDSStores;

/*!
 * @typedef JDSLoaderCallback
 *
 * This block is called by the JDS data loader to report progess and to allow early termination
 * of the load operation.
 *
 * @param total The number of records that will be processed by this data load operation
 * @param processed The number of records that have already been processed (0 <= processed <= total).
 * @return @c YES if the load should continue, @c NO if the load should be cancelled.
 */
typedef BOOL (^JDSLoaderCallback)(NSInteger total NS_SWIFT_NAME(total),
                                  NSInteger processed NS_SWIFT_NAME(processed));

/*!
 * Define the JDS Loader API which is complemented by the other APIs.
 *
 * Since the JDS Data Loader will require Store definitions and a Loadset, this
 * API is directly connected also the JDSLoadsetAPI and the JDSStoreAPI. As a
 * result do these APIs offer convenience functions to trigger the load directly
 * froma given JDSStores instance or a JDSLoadsetBunbdle instance.
 */
@interface JDSLoaderAPI : NSObject

/*!
 * Process a loadset bundle and applies its content to a provided store
 * using a provided schema specification. If the loadset contains a full
 * load, then the associated store will be initialized with a new database,
 * in case of an incremental update, the associated store must contain the
 * matching baseline data, otherwise the data load will fail.
 *
 * @param loadsetPath The path to the loadset bundle which either can be a
 *        folder containing the loadset data, or a ZIP archive with the
 *        loadset bundle data.
 * @param schema The data schema to be used to apply the loadset. It will be
 *        verified against the loadset schema specification, and the schema
 *        of the store in case of an incremental update.
 * @param storeConfigs The configuration of the stores that carry the data
 *        for each module in the schema. Since a loadset will always
 *        only contain data for one module, it would be sufficient to supply
 *        only a store configuration for that module.
 * @param callback A callback that receives progress updates and allows the
 *        caller to terminate the load processed by returning false.
 * @param error Will receive detailed error information in case the load
 *        fails for any reason. Also a premature termination by the caller
 *        will be reported as an error (JDSStatusCancelled).
 * @return @c YES if the operation succeeded. @c NO if an error occurred. In that case
 *         the error argument would contain additional information.
 */
+(BOOL) loadBundle:(NSString*_Nonnull)loadsetPath
        withSchema:(JDSSchema*_Nonnull)schema
         andStores:(JDSStores*_Nonnull)storeConfigs
          callback:(JDSLoaderCallback _Nullable)callback
             error:(NSError*_Nullable*_Nullable)error;

/*!
 * Process a loadset bundle and applies its content to a provided store
 * using a provided schema specification. If the loadset contains a full
 * load, then the associated store will be initialized with a new database,
 * in case of an incremental update, the associated store must contain the
 * matching baseline data, otherwise the data load will fail.
 *
 * @param loadsetPath The path to the loadset bundle which either can be a
 *        folder containing the loadset data, or a ZIP archive with the
 *        loadset bundle data.
 * @param schema The data schema to be used to apply the loadset. It will be
 *        verified against the loadset schema specification, and the schema
 *        of the store in case of an incremental update.
 * @param storeConfigs The configuration of the stores that carry the data
 *        for each module in the schema. Since a loadset will always
 *        only contain data for one module, it would be sufficient to supply
 *        only a store configuration for that module.
 * @param memoryLimit Allows providing a rough upper limit for the size of
 *        records being held concurrently in memory during the load. This limit
 *        might temporally be exceeded and should only be considered a recommendation
 *        for the loader. If the value is 0 then no limit is applied.
 * @param callback A callback that receives progress updates and allows the
 *        caller to terminate the load processed by returning false.
 * @param error Will receive detailed error information in case the load
 *        fails for any reason. Also a premature termination by the caller
 *        will be reported as an error (JDSStatusCancelled).
 * @return @c YES if the operation succeeded. @c NO if an error occurred. In that case
 *         the error argument would contain additional information.
 */
+(BOOL) loadBundle:(NSString*_Nonnull)loadsetPath
        withSchema:(JDSSchema*_Nonnull)schema
         andStores:(JDSStores*_Nonnull)storeConfigs
    andMemoryLimit:(NSUInteger)memoryLimit
          callback:(JDSLoaderCallback _Nullable)callback
             error:(NSError*_Nullable*_Nullable)error;

@end
