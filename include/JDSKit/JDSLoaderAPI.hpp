///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSLoaderAPI.h
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        Define the C++ JDS Loader API
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#ifdef __cplusplus

#include <functional>

#include "JDSSchema.hpp"
#include "JDSCommons.hpp"
#include "JDSStoreAPI.hpp"

namespace jds
{
    /*!
     * @typedef LoadCallback
     *
     * This block is called by the JDS data loader to report progess and to allow early termination
     * of the load operation.
     *
     * @param total The number of records that will be processed by this data load operation
     * @param processed The number of records that have already been processed (0 <= processed <= total).
     * @return @c true if the load should continue, @c false if the load should be cancelled.
     */
    typedef std::function<bool(const size_t total, const size_t processed)> LoadCallback;
    
    /*!
     * Process a loadset bundle and applies its content to a provided store
     * using a provided schema specification. If the loadset contains a full
     * load, then the associated store will be initialized with a new database,
     * in case of an incremental update, the associated store must contain the
     * matching baseline data, otherwise the data load will fail.
     *
     * @param schema The data schema to be used to apply the loadset. It will be
     *        verified against the loadset schema specification, and the schema
     *        of the store in case of an incremental update.
     * @param bundlePath The path to the loadset bundle which either can be a
     *        folder containing the loadset data, or a ZIP archive with the
     *        loadset bundle data.
     * @param storeConfigs The configuration of the stores that carry the data
     *        for each module in the schema. Since a loadset will always
     *        only contain data for one module, it would be sufficient to supply
     *        only a store configuration for that module.
     * @param callback A callback that receives progress updates and allows the
     *        caller to terminate the load processed by returning false.
     * @return @c JDS_OK if the operation succeeded or any other JDSStatus if the
     *         operation was cancelled by the user or an error occurred.
     */
    JDSStatus loadBundle(const Schema& schema,
                         std::string bundlePath,
                         const StoreConfigurations& storeConfigs,
                         LoadCallback callback);

    /*!
     * Process a loadset bundle and applies its content to a provided store
     * using a provided schema specification. If the loadset contains a full
     * load, then the associated store will be initialized with a new database,
     * in case of an incremental update, the associated store must contain the
     * matching baseline data, otherwise the data load will fail.
     *
     * @param schema The data schema to be used to apply the loadset. It will be
     *        verified against the loadset schema specification, and the schema
     *        of the store in case of an incremental update.
     * @param bundlePath The path to the loadset bundle which either can be a
     *        folder containing the loadset data, or a ZIP archive with the
     *        loadset bundle data.
     * @param storeConfigs The configuration of the stores that carry the data
     *        for each module in the schema. Since a loadset will always
     *        only contain data for one module, it would be sufficient to supply
     *        only a store configuration for that module.
     * @param memoryLimit Allows providing a rough upper limit for the size of
     *        records being held concurrently in memory during the load. This limit
     *        might temporally be exceeded and should only be considered a recommendation
     *        for the loader. If the value is 0 then no limit is applied.
     * @param callback A callback that receives progress updates and allows the
     *        caller to terminate the load processed by returning false.
     * @return @c JDS_OK if the operation succeeded or any other JDSStatus if the
     *         operation was cancelled by the user or an error occurred.
     */
    JDSStatus loadBundle(const Schema& schema,
                         std::string bundlePath,
                         const StoreConfigurations& storeConfigs,
                         size_t memoryLimit,
                         LoadCallback callback);
}

#endif
