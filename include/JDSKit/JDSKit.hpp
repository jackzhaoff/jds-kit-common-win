///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSKit.hpp
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        The C++ umbrella header for JDSKit
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#include <JDSKit/JDSCommons.hpp>
#include <JDSKit/JDSException.hpp>
#include <JDSKit/JDSSchema.hpp>
#include <JDSKit/JDSVersionInfo.hpp>

#include <JDSKit/JDSLoaderAPI.hpp>
#include <JDSKit/JDSStoreAPI.hpp>
#include <JDSKit/JDSDataAccessAPI.hpp>
#include <JDSKit/JDSLoadsetAPI.hpp>

#include <JDSKit/JDSTypes.hpp>
#include <JDSKit/JDSExtent.hpp>
#include <JDSKit/JDSConnection.hpp>
#include <JDSKit/JDSLoaderConnection.hpp>

