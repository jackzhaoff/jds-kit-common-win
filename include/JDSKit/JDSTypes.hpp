///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSTypes.hpp
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        Define the C++ data types representing the basic types in a
//        loadset.
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#ifdef __cplusplus

#include <vector>
#include <string>

namespace jds
{
    /*!
     * Numeric value
     */
    class Numeric
    {
    public:
        Numeric() {}
        
        Numeric(std::string value) :
            m_value(value)
        {}
        
    public:
        std::string numericString() const
        {
            return m_value;
        }
        
        double real() const;
        int integer() const;
        long longinteger() const;

        const size_t memoryLoad() const
        {
            return m_value.size();
        }

    protected:
        std::string     m_value;
    };

    /*!
     * Date value
     */
    class Date
    {
    public:
        Date() {}
        
        Date(std::string year, std::string month, std::string day) :
            m_year(year),
            m_month(month),
            m_day(day) {}
        
    public:
        std::string dateString() const
        {
            return m_year + "-" + m_month + "-" + m_day;
        }

    protected:
        std::string     m_year;
        std::string     m_month;
        std::string     m_day;
    };

    /*!
     * A Time value
     */
    class Time
    {
    public:
        Time() {};
        
        Time(std::string hour, std::string minutes, std::string seconds, std::string millis) :
            m_hour(hour),
            m_minutes(minutes),
            m_seconds(seconds),
            m_millis(millis) {}
        
    public:
        std::string timeString() const
        {
            return m_hour + ":" + m_minutes + ":" + m_seconds + "." + m_millis;
        }

    protected:
        std::string     m_hour;
        std::string     m_minutes;
        std::string     m_seconds;
        std::string     m_millis;
    };

    /*!
     * A DateTime value is a combination of a Date and a Time value and
     * thus serves also as a TimeStamp value.
     */
    class DateTime
    {
    public:
        DateTime() {};
        
        DateTime(Date& date, Time& time) :
            m_date(date),
            m_time(time) {}
        
    public:
        std::string datetimeString() const
        {
            return m_date.dateString() + " " + m_time.timeString();
        }
        
    protected:
        Date        m_date;
        Time        m_time;
    };

    /*!
     * Store geometry bounds. These are computed on the fly as the
     * loadsets are processed and will then be persisted in the JDS
     * store.
     */
    class Bounds
    {
    public:
        Bounds();
        
        void includePoint(const double& x, const double& y);
        
        inline const bool isNull() const { return m_isNull; }
        inline const double& min_x() const { return m_xmin; }
        inline const double& max_x() const { return m_xmax; }
        inline const double& min_y() const { return m_ymin; }
        inline const double& max_y() const { return m_ymax; }
        
    protected:
        bool    m_isNull;
        double  m_xmin;
        double  m_xmax;
        double  m_ymin;
        double  m_ymax;
    };

    /*!
     * Represent a binary encoded Geometry feature in JDS
     */
    class Geometry
    {
    protected:
        bool                        m_isNull;
        std::vector<char>           m_data;
        
    public:
        Geometry();
        Geometry(std::vector<char> blob);
        Geometry(const Geometry& other);
        
        virtual ~Geometry() { }

        Geometry& operator =(const Geometry& other);
        
        const bool isNull() const;
        
        const std::vector<char>& data() const;
        const Bounds bounds() const;
        
        virtual const size_t memoryLoad() const
        {
            return m_data.size();
        }
    };
}

#endif
