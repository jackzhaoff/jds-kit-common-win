///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSVersionInfo.hpp
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        Provide definition of the VersionInfo class
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#ifdef __cplusplus

#include <string>
#include <memory>

#include <JDSKit/JDSCommons.hpp>

namespace jds
{
    /*!
     * @class VersionInfo
     *
     * Wrap the version information from a loadset or a store and
     * allows the creation of a version string based on that information.
     *
     * Furthermore defined equality operator to check if two versions are
     * the same resp. compatible.
     */
    class VersionInfo
    {
    public:
        VersionInfo(const std::string& name,
                    int model,
                    optional<int> revision,
                    optional<int> addition,
                    optional<std::string> qualifier);

        VersionInfo(const std::string& name,
                    int model,
                    int revision,
                    int addition);

        VersionInfo(const std::string& name,
                    int model,
                    int revision,
                    int addition,
                    std::string qualifier);

        VersionInfo(const std::string& name,
                    int model,
                    int revision,
                    std::string qualifier);

        VersionInfo(const std::string& name,
                    int model,
                    int revision);
        
        VersionInfo(const std::string& name,
                    int model);

        VersionInfo(const std::string& name,
                    int model,
                    std::string qualifier);

        VersionInfo(const VersionInfo& other);
        
        ~VersionInfo() {}
        
    public:
        const std::string name() const;

        const int model() const;
        
        const optional<int> revision() const;
        const optional<int> addition() const;
        const optional<std::string> qualifier() const;

        VersionInfo& operator =(const VersionInfo& other);
        
        const bool operator == (const VersionInfo& other) const;
        
        const std::string string(bool withName = true) const;
        
    protected:
        std::string             m_name;
        int                     m_model;
        
        optional<int>           m_revision;
        optional<int>           m_addition;
        optional<std::string>   m_qualifier;
    };
        
}

#endif

