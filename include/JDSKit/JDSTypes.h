///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSTypes.h
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        Shared data types for the JDS APIs
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


#pragma once

#import <Foundation/Foundation.h>

/*!
 * @class JDSVersion
 *
 * Stores information about the data version of a loadset or a
 * loaded data store.
 */
@interface JDSVersion : NSObject

/*! The name of the schema module associated with this version */
@property (nonatomic, readonly) NSString* _Nonnull name;

/*! The model version identification */
@property (nonatomic, readonly) NSNumber* _Nonnull model;

/*! The optional revision identification */
@property (nonatomic, readonly) NSNumber* _Nullable revision;

/*! The optional addition identification */
@property (nonatomic, readonly) NSNumber* _Nullable addition;

/*! The optional qualifier identification */
@property (nonatomic, readonly) NSString* _Nullable qualifier;

/*!
 * Render the version information as string. The returned string
 * will not contain the module name.
 *
 * @return The formatted version string.
 */
-(NSString* _Nonnull) text;

/*!
 * Render the version information as string. The returned string
 * will contain the module name if requested.
 *
 * @param withName If @YES, then the module name will be included in the
 *        generated string.
 * @return The formatted version string.
 */
-(NSString* _Nonnull) textWithName:(BOOL)withName;

@end

/*!
 * Store effectivity information as returned from loadsets and
 * loaded data stores.
 */
@interface JDSEffectivity : NSObject

@property (nonatomic, readonly) long dataVersion;

@property (nonatomic, readonly) NSNumber* _Nullable dataRevision;
@property (nonatomic, readonly) NSNumber* _Nullable effectiveFrom;
@property (nonatomic, readonly) NSNumber* _Nullable effectiveTo;

@end

/*!
 * Wrapper for the JDSSchema instance. This is used by the
 * implementations of a JDSSchema to provide an Objective C object.
 */
@interface JDSSchema : NSObject

/*!
 * Create a JDSSchema instance
 *
 * @param schema A pointer to the static JDSSchema instance.
 * @return The JDSSchema instance wrapping the schema instance.
 */
+(JDSSchema* _Nonnull )createWithSchema:(void* _Nonnull)schema;

/*! Schema version */
@property (nonatomic, readonly) JDSVersion* _Nonnull version;

@end





