///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSStatus.h
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        Define the C++ JDS Status Codes
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#ifdef __cplusplus
namespace jds
{
#endif
	typedef int     JDSStatus;

	#define JDS_STATUS(category, id)      (((category) | ((id) & 0xffff)))

    
    #define JDS_CAT_GENERAL         0
    
    #define JDS_CAT_LOADSET         0x10000
    #define JDS_CAT_STORE           0x20000
    #define JDS_CAT_ZIP             0x30000
    #define JDS_CAT_DB              0x40000
    #define JDS_CAT_SCHEMA          0x50000
    
    /*! Indicates a successful execution */
    #define JDS_OK                  JDS_STATUS(JDS_CAT_GENERAL, 0)

    /*! Indicates that the data load was terminated by user request */
    #define JDS_CANCELLED           JDS_STATUS(JDS_CAT_GENERAL, 1)
    
    /*! Indicates a successful execution */
    #define JDS_E_GENERAL           JDS_STATUS(JDS_CAT_GENERAL, 2)

    /*! The content of a loadset could not be processed */
    #define JDS_E_LS_CONTENT        JDS_STATUS(JDS_CAT_LOADSET, 3)

    /*! The ZIP archive of a loadset could not be expanded */
    #define JDS_E_ZIP_FORMAT        JDS_STATUS(JDS_CAT_ZIP,     4)
    
    /*! Target store serial number is does not match expected baseline in delta loadset */
    #define JDS_E_LS_BASELINE       JDS_STATUS(JDS_CAT_LOADSET, 5)
    
    /*! Loadset contains data for unsupported module */
    #define JDS_E_LS_MODULE         JDS_STATUS(JDS_CAT_LOADSET, 6)
    
    /*! No effectivity information found in store */
    #define JDS_E_STORE_EFFECTIVITY JDS_STATUS(JDS_CAT_STORE,   7)
    
    /*! Provided path to loadset bundle is no directory */
    #define JDS_E_LS_NODIR          JDS_STATUS(JDS_CAT_LOADSET, 8)

    /*! Provided path to loadset bundle is no directory */
    #define JDS_E_LS_NOZIP          JDS_STATUS(JDS_CAT_LOADSET, 9)
    
    /*! Requested entry not found in loadset bundle */
    #define JDS_E_LS_NOENTRY        JDS_STATUS(JDS_CAT_LOADSET, 10)
    
    #define JDS_E_ZIP_EXTRACT       JDS_STATUS(JDS_CAT_ZIP,     11)
    #define JDS_E_DB_SEQUENCE       JDS_STATUS(JDS_CAT_DB,      12)
    
    /*! Schema does not contain the requested module */
    #define JDS_E_SCHEMA_MODULE     JDS_STATUS(JDS_CAT_SCHEMA,  13)
    
    /*! The loadset is of an unsupported version */
    #define JDS_E_LS_VERSION        JDS_STATUS(JDS_CAT_LOADSET, 14)
    
    /*! The loadset metadata could not be validated */
    #define JDS_E_LS_CHECKS         JDS_STATUS(JDS_CAT_LOADSET, 15)
    
    /*! No store available for requested module name */
    #define JDS_E_STORE_MODULE      JDS_STATUS(JDS_CAT_STORE,   16)

    /*! No store found at given location */
    #define JDS_E_NO_STORE          JDS_STATUS(JDS_CAT_STORE,   17)

    /*! Schema implementation uses an unsupported API version */
    #define JDS_E_API_UNSUPPORTED   JDS_STATUS(JDS_CAT_GENERAL, 18)
    
    /*! Store validation failed. No further details are provided */
    #define JDS_E_STORE_INVALID     JDS_STATUS(JDS_CAT_STORE,   19)

    /*! Error occurred during asynchronous execution */
    #define JDS_E_ASYNC             JDS_STATUS(JDS_CAT_GENERAL, 20)

    /*! General exception caught in database access */
    #define JDS_E_DB                JDS_STATUS(JDS_CAT_DB,      21)

#ifdef __cplusplus
}
#endif
