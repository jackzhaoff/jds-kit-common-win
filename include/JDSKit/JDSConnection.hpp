///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSConnection.hpp
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        Simple wrapper for SQLite that offers a synchronous API. This
//        implementation serves as the basis for the asynchronous API
//        provided by JDSLoaderConnection.hpp.
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#ifdef __cplusplus

#include <string>
#include <memory>
#include <exception>
#include <algorithm>
#include <cstring>
#include <cstdint>
#include <vector>

#include <functional>

#include "JDSException.hpp"

typedef struct sqlite3 sqlite3;
typedef struct sqlite3_stmt sqlite3_stmt;

namespace jds
{
    // Exception thrown on error
    class DBException : public JDSException
    {
    public:
        DBException(int c) :
            JDSException(JDS_STATUS(JDS_CAT_DB, c), createMessage(c)) {}

        DBException(int c, const char* sql, const char* msg = nullptr) :
            JDSException(JDS_STATUS(JDS_CAT_DB, c), createMessage(c, sql, msg)) {}

        DBException(int c, sqlite3_stmt *pStmt, const char* msg = nullptr) :
            JDSException(JDS_STATUS(JDS_CAT_DB, c), createMessage(c, pStmt, msg)) {}
        
        virtual ~DBException() throw ();
        
    private:
        std::string createMessage(int code);
        
        std::string createMessage(int code, const char* sql, const char* msg = nullptr);

        std::string createMessage(int code, sqlite3_stmt *pStmt, const char* msg = nullptr);
    };
    
    class DBStatement;
    
    /*!
     * Wraps the connection to the SQLite database
     */
    class DBConnection
    {
        friend class DBStatement;
        
    public:
        DBConnection();
        
        DBConnection(const std::string& filename, const std::string& cipher = "");
        DBConnection(const char *filename, const char* cipher = nullptr);

        DBConnection(const DBConnection& db);

        ~DBConnection();
        
        /*!
         * Set a soft upper memory limit. SQLite may not always be able to stay below that limit
         * but will strive to stay within the set limits.
         *
         * @param limit The memory limit in bytes. If zero, no upper limit is applied.
         *
         * @return Previously set memory limit.
         */
        size_t set_memory_limit(const size_t limit);

        void close();
        
        DBConnection& operator=(const DBConnection&) = delete;
        
        void swap(DBConnection& other);
        
        DBConnection(DBConnection&& other);
        
        DBConnection& operator=(DBConnection&& other);
        
        void cryptKey(const std::string& key);
        
        void beginTransaction();
        
        void commit();
        
        void rollback();

        /*!
         * Retrieve the raw SQLite3 database connection handle. This function
         * is similar to the get() functions besides that ownership of the handle
         * is passed to the caller. Once the DBConnection is destroyed it will not
         * close the underlying connection.
         *
         * @return The raw sqlite3 handle.
         */
        ::sqlite3 *take();

        /*!
         * Retrieve the raw SQLite3 database connection handle.
         *
         * @return The raw sqlite3 handle.
         */
        ::sqlite3 *get();

        /*!
         * Retrieve the const raw SQLite3 database connection handle.
         *
         * @return The const raw sqlite3 handle.
         */
        const ::sqlite3 *get() const;
        
        // Number of changes due to the most recent DBStatement.
        unsigned int changes();
        
        // Execute a simple DBStatement
        void execute(const std::string& sql);
        void execute(const char *sql);
        
        DBStatement prepare(std::string sql);

    protected:
        void prepareDBConnection();
        
        void detachTables();

    private:
        sqlite3*        m_db;
        bool            m_owner;
    };
    
    
    class DBBinder
    {
    public:
        DBBinder();
        
        DBBinder(DBStatement& DBStatement);
        
        ~DBBinder();
        
        void swap(DBBinder& other);
        
        DBBinder& blob(unsigned int i, const std::vector<char>& data);
        DBBinder& blob_ref(unsigned int i, const void *data, size_t len);
        
        DBBinder& real(unsigned int i, double value);
        DBBinder& int32(unsigned int i, int32_t value);
        
        DBBinder& int64(unsigned int i, int64_t value);
        DBBinder& null(unsigned int i);
        
        DBBinder& text(unsigned int i, const char *orig);
        DBBinder& text(unsigned int i, const std::string& value);
        DBBinder& text_ref(unsigned int i, const std::string& value);
        DBBinder& text_ref(unsigned int i, const char *value);
        
        // Automatic indexing
        DBBinder& blob(const std::vector<char>& data);
        DBBinder& blob_ref(const void *data, size_t len);
        
        DBBinder& real(double value);
        DBBinder& int32(int32_t value);
        
        DBBinder& int64(int64_t value);
        DBBinder& null();
        
        DBBinder& text(const char *orig);
        DBBinder& text(const std::string& value);
        DBBinder& text_ref(const std::string& value);
        DBBinder& text_ref(const char *value);
        
        void clear();
        
    private:
        unsigned int                    m_index;
        sqlite3_stmt*                   m_stmt;
    };
    
    
    class DBReader
    {
    public:
        DBReader();
        
        DBReader(DBStatement& s);
        
        DBReader(const DBReader& other);
        
        void swap(DBReader& other);
        
        bool null(unsigned int i);
        const std::vector<char> blob(unsigned int i);
        double real(unsigned int i);
        int64_t int64(unsigned int i);
        int32_t int32(unsigned int i);
        const char *cstr(unsigned int i);
        std::string text(unsigned int i);
        
    private:
        sqlite3_stmt*   m_stmt;
    };
    
    
    // DBStatement
    class DBStatement
    {
        friend class DBReader;
        friend class DBBinder;
        
    public:
        DBStatement();
        DBStatement(DBConnection& db, const char *sql);
        DBStatement(DBConnection& db, const std::string& sql);

        ~DBStatement();
        
        DBStatement(const DBStatement&) = delete;
        DBStatement& operator=(const DBStatement&) = delete;
        
        void swap(DBStatement& other);
        
        DBStatement(DBStatement&& r);
        DBStatement& operator=(DBStatement&& other);
        
        sqlite3_stmt *get();
        const sqlite3_stmt *get() const;
        
        typedef std::function<void(DBBinder& binder)> BinderCallback;
        typedef std::function<bool(DBReader& reader)> ReaderCallback;

        void bind(BinderCallback callback);
        
        bool step();
        
        void execute();

        bool execute(ReaderCallback reader);
        
        void reset();
        void clearBindings();
        
        DBReader row();
        
        const std::vector<char> blob(unsigned int i);

        bool null(unsigned int i);
        double real(unsigned int i);
        int64_t int64(unsigned int i);
        int32_t int32(unsigned int i);
        const char *cstr(unsigned int i);
        std::string text(unsigned int i);

    private:
        sqlite3_stmt*       m_stmt;
    };
}

#endif
