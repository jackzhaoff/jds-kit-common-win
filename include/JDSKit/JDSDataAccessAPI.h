///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSDataAccessAPI.h
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        The JDS Data Access API
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


#pragma once

#import <JDSKit/JDSKit.h>

/*!
 * @typedef JDSRecordModificationType
 *
 * Type modification that occurred to a given record during last data update
 */
typedef NSString * const JDSRecordModificationType NS_STRING_ENUM;

/*! Type of modification could not be determined */
FOUNDATION_EXPORT JDSRecordModificationType _Nonnull JDSRecordModificationTypeUnknown;

/*! The record was removed during last data update */
FOUNDATION_EXPORT JDSRecordModificationType _Nonnull JDSRecordModificationTypeRemoved;

/*! The record was inserted or updated during last data update */
FOUNDATION_EXPORT JDSRecordModificationType _Nonnull JDSRecordModificationTypeStored;

/*!
 * @typedef JDSPartModificationType
 *
 * Type modification that occurred to a given part during last data update
 */
typedef NSString * const JDSPartModificationType NS_STRING_ENUM;

/*! The part was removed during last data update */
FOUNDATION_EXPORT JDSPartModificationType _Nonnull JDSPartModificationTypeRemoved;

/*! The part was inserted or updated during last data update */
FOUNDATION_EXPORT JDSPartModificationType _Nonnull JDSPartModificationTypeAdded;

/*!
 * @class JDSBinder
 *
 * The JDSBinder is used to provide values for parameter placeholders in a prepared
 * SQL statement. Therefore the JDSBinder maintains an automatic index of the parameters
 * that is incremented every time a parameter value is bound. This way, the parameters
 * can be populated in a left-to-right manner in the order of occurrence in the compiled
 * SQL statement.
 *
 * In addition to that allows the binder to directly set a particular parameter value using
 * the column index of the parameter (important to note is that the index of the first
 * parameter is 1 in contrast to the JDSRow interface where the first column is indexed with
 * zero).
 */
@interface JDSBinder : NSObject

/*!
 * Set the column value to NULL.
 *
 * @param columnIndex Index of the column to set this value  (first parameter has index 1)
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) nullAt:(NSInteger)columnIndex;

/*!
 * Set the column value of the current column to NULL.
 * The current column is then moved to the next column.
 *
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) null;

/*!
 * Set the column value to a binary value (BLOB).
 *
 * @param data The NSData instance containing the binary data.
 * @param columnIndex Index of the column to set this value (first parameter has index 1)
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) blob:(NSData* _Nonnull)data at:(NSInteger)columnIndex;

/*!
 * Set the column value of the current column to a string value.
 * The current column is then moved to the next column.
 *
 * @param data The NSString instance containing the text to set.
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) blob:(NSData* _Nonnull)data;

/*!
 * Set the column value of the selected column to a string value.
 *
 * @param data The NSData instance containing the binary data.
 * @param columnIndex Index of the column to set this value  (first parameter has index 1)
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) string:(NSString* _Nonnull)data at:(NSInteger)columnIndex;

/*!
 * Set the column value of the current column to a binary value (BLOB).
 * The current column is then moved to the next column.
 *
 * @param data The NSData instance containing the binary data.
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) string:(NSString* _Nonnull)data;

/*!
 * Set the column value of the selected column to a floating point value.
 *
 * @param value The NSNumber value to be used as double value.
 * @param columnIndex Index of the column to set this value  (first parameter has index 1)
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) real:(NSNumber* _Nonnull)value at:(NSInteger)columnIndex;

/*!
 * Set the column value of the current column to a floating point value.
 * The current column is then moved to the next column.
 *
 * @param value The NSNumber value to be used as double value.
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) real:(NSNumber* _Nonnull)value;

/*!
 * Set the column value of the selected column to a double value.
 *
 * @param value The double value to be used.
 * @param columnIndex Index of the column to set this value (first parameter has index 1)
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) double:(double)value at:(NSInteger)columnIndex;

/*!
 * Set the column value of the current column to a double value.
 * The current column is then moved to the next column.
 *
 * @param value The double value to be assigned.
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) double:(double)value;

/*!
 * Set the column value of the selected column to an integer value.
 *
 * @param value The NSNumber value to be used as integer value.
 * @param columnIndex Index of the column to set this value (first parameter has index 1)
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) int:(NSNumber* _Nonnull)value at:(NSInteger)columnIndex;

/*!
 * Set the column value of the current column to an integer value.
 * The current column is then moved to the next column.
 *
 * @param value The NSNumber value to be used as integer value.
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) int:(NSNumber* _Nonnull)value;

/*!
 * Set the column value of the selected column to a 32 bit integer value.
 *
 * @param value The 32 bit integer value to be assigned.
 * @param columnIndex Index of the column to set this value (first parameter has index 1)
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) int32:(int32_t)value at:(NSInteger)columnIndex;

/*!
 * Set the column value of the current column to a 32 bit integer value.
 * The current column is then moved to the next column.
 *
 * @param value The 32 bit integer value to be assigned.
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) int32:(int32_t)value;

/*!
 * Set the column value of the selected column to a long value.
 *
 * @param value The NSNumber value to be used as long value.
 * @param columnIndex Index of the column to set this value (first parameter has index 1)
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) long:(NSNumber* _Nonnull)value at:(NSInteger)columnIndex;

/*!
 * Set the column value of the current column to a long value.
 * The current column is then moved to the next column.
 *
 * @param value The NSNumber value to be used as long value.
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) long:(NSNumber* _Nonnull)value;

/*!
 * Set the column value of the selected column to a 64 bit integer value.
 * The current column is then moved to the next column.
 *
 * @param value The 64 bit integer value to be assigned.
 * @param columnIndex Index of the column to set this value (first parameter has index 1)
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) int64:(int64_t)value at:(NSInteger)columnIndex;

/*!
 * Set the column value of the current column to a 64 bit integer value.
 * The current column is then moved to the next column.
 *
 * @param value The 64 bit integer value to be assigned.
 * @return The binder instance so that commands can be chained.
 */
-(JDSBinder* _Nonnull) __attribute__((ns_returns_retained)) int64:(int64_t)value;

/**
 * Reset all bound values and move the current column back to the start
 */
-(void) clear;

@end

/*!
 * @class JDSRow
 *
 * The JDSRow is used to retrieve values returned from a query. Each result is made accessible
 * through the JDSRow interface. The callback of the @c executeQuery method will provide a JDSRow
 * instance as its only parameter.
 */
@interface JDSRow : NSObject

/*!
 * Determine, if the selected column is NULL.
 *
 * @param columnIndex Index of the column to get value from (first column has index 0)
 * @return @c YES if the column is NULL, @c NO otherwise.
 */
-(BOOL)null:(NSInteger)columnIndex;

/*!
 * Return the binary data in the selected column as NSData instance.
 *
 * @param columnIndex Index of the column to get value from (first column has index 0)
 * @return NSData instance with the binary data loaded.
 */
-(NSData* _Nullable)  __attribute__((ns_returns_retained)) blob:(NSInteger)columnIndex;

/*!
 * Return the value in the selected column as NSNumber instance with a double value.
 *
 * @param columnIndex Index of the column to get value from (first column has index 0)
 * @return NSNumber instance with the double value.
 */
-(NSNumber*_Nullable) __attribute__((ns_returns_retained)) real:(NSInteger)columnIndex;

/*!
 * Return the value in the selected column as NSNumber instance with a long value.
 *
 * @param columnIndex Index of the column to get value from (first column has index 0)
 * @return NSNumber instance with the long value.
 */
-(NSNumber*_Nullable) __attribute__((ns_returns_retained)) long:(NSInteger)columnIndex;

/*!
 * Return the value in the selected column as NSNumber instance with a int value.
 *
 * @param columnIndex Index of the column to get value from (first column has index 0)
 * @return NSNumber instance with the int value.
 */
-(NSNumber*_Nullable) __attribute__((ns_returns_retained)) int:(NSInteger)columnIndex;

/*!
 * Return the value in the selected column as NSString instance.
 *
 * @param columnIndex Index of the column to get value from (first column has index 0)
 * @return NSString instance with the value.
 */
-(NSString*_Nullable) __attribute__((ns_returns_retained)) string:(NSInteger)columnIndex;

/*!
 * Return the value in the selected column as double value.
 *
 * @param columnIndex Index of the column to get value from (first column has index 0)
 * @return The double value.
 */
-(double)double:(NSInteger)columnIndex;

/*!
 * Return the value in the selected column as int64 value.
 *
 * @param columnIndex Index of the column to get value from (first column has index 0)
 * @return The int64 value.
 */
-(int64_t)int64:(NSInteger)columnIndex;

/*!
 * Return the value in the selected column as int32 value.
 *
 * @param columnIndex Index of the column to get value from (first column has index 0)
 * @return The int32 value.
 */
-(int32_t)int32:(NSInteger)columnIndex;

@end

/*!
 * @typedef JDSResultCallback
 *
 * @param row The JDSRow instance carrying the data for the current row.
 * @return Indicate, if the traversal should continue @c YES, or if the traversal should
 *         be terminated @c NO.
 */
typedef BOOL (^JDSResultCallback)(JDSRow*_Nonnull row NS_SWIFT_NAME(row));

/*!
 * @typedef JDSBindingCallback
 *
 * @param binder The DJSBinder instance mapping the values to the underlying statement.
 */
typedef void (^JDSBindingCallback)(JDSBinder*_Nonnull binder NS_SWIFT_NAME(binder));

/*!
 * @typedef JDSRecordModificationInfoCallback
 *
 * @param ordinal The type identifier of the actual record.
 * @param identifier The identifier of the actual record.
 * @param modification Provides the type of modification of the actual record
 * @return Indicate, if the traversal should continue @c YES, or if the traversal should
 *         be terminated @c NO.
 */
typedef BOOL (^JDSRecordModificationInfoCallback)(NSUInteger ordinal, NSUInteger identifier, JDSRecordModificationType _Nonnull modification);


/*!
 * @typedef JDSPartModificationInfoCallback
 *
 * @param part The name of the part with modifications.
 * @param modification Provides the type of modification of the actual record
 * @return Indicate, if the traversal should continue @c YES, or if the traversal should
 *         be terminated @c NO.
 */
typedef BOOL (^JDSPartModificationInfoCallback)(NSString* _Nonnull part, JDSPartModificationType _Nonnull modification);


/*!
 * @class JDSStatement
 */
@interface JDSStatement : NSObject

/*!
 * Reset the prepared statement so that it can be re-used
 *
 * @return YES if the operation succeeded, NO otherwise. If NO is returned,
 *         the information in the error object should be checked for details.
 */
-(BOOL)reset:(NSError *_Nullable __autoreleasing *_Nullable)error;

/*!
 * Execute the prepared statement without expecting any data to be returned.
 *
 * @param error Optional NSError reference to store information about any
 *        errors that occurred during the execution.
 * @return YES if the operation succeeded, NO otherwise. If NO is returned,
 *         the information in the error object should be checked for details.
 */
-(BOOL)execute:(NSError *_Nullable __autoreleasing *_Nullable)error;

/*!
 * Execute the prepared statement that is expected to returne data.
 *
 * @code
 * let stmt = try! db.prepare("SELECT title, sku FROM [Products] WHERE title LIKE ? AND category = ?");
 *
 * try! stmt.bind({ (binder) in
 *        binder
 *          .string(filter)
 *          .int32(category);
 *      });
 *
 * try! stmt.executeQuery({ (row) -> Bool in
 *      print(row.string(0), row.int32(1));
 * });
 * @endcode
 *
 * @param callback The callback block that is called for each row returned
 *        unless the block returns @c NO or no additional records are found.
 * @param error Optional NSError reference to store information about any
 *        errors that occurred during the execution.
 * @return @c YES if the operation succeeded, @c NO otherwise. If @c NO is returned,
 *         the information in the error object should be checked for details.
 */
-(BOOL)executeQuery:(JDSResultCallback _Nonnull)callback
              error:(NSError *_Nullable __autoreleasing *_Nullable)error;

/*!
 * Bind values to the parameter placeholders in this statement.
 *
 * This calls a block with a JDSBinder instance to perform the actual
 * binding of the values.
 *
 * @code
 * let stmt = try! db.prepare("SELECT title, sku FROM [Products] WHERE title LIKE ? AND category = ?");
 *
 * try! stmt.bind({ (binder) in
 *        binder
 *          .string(filter)
 *          .int32(category);
 *      });
 * @endcode
 *
 * @param binder The callback block to perform the binding.
 * @param error Optional NSError reference to store information about any
 *        errors that occurred during the execution.
 * @return YES if the operation succeeded, NO otherwise. If NO is returned,
 *         the information in the error object should be checked for details.
 */
-(BOOL)bind:(JDSBindingCallback _Nonnull)binder
      error:(NSError *_Nullable __autoreleasing *_Nullable)error;

@end

/*!
 * Wrapper for the SQLite Connection object
 */
@interface JDSConnection : NSObject

/*! The raw SQLite3 connection handle */
-(void* _Nonnull) db;

/*! The raw SQLite3 connection handle and the caller is taking ownership of the connection */
-(void* _Nonnull) take;

/*!
 * Prepare a SQL statement
 *
 * @param sql The SQL statement to prepare
 * @param error Optional NSError reference to store information about any
 *        errors that occurred during the execution.
 * @return A prepared JDSStatement instance in case of success or nil if the
 *         operation failed.
 */
-(JDSStatement* _Nullable) __attribute__((ns_returns_retained)) prepare:(NSString*_Nonnull)sql
                            error:(NSError *_Nullable __autoreleasing *_Nullable)error;

/*!
 * Execute a SQL statement against the underlying connection
 *
 * @param sql The SQL statement to execute
 * @param error Optional NSError reference to store information about any
 *        errors that occurred during the execution.
 * @return YES if the operation succeeded, NO otherwise. If NO is returned,
 *         the information in the error object should be checked for details.
 */
-(BOOL)execute:(NSString*_Nonnull)sql
         error:(NSError *_Nullable __autoreleasing *_Nullable)error;

/*!
 * Retrieve the type of modification that happened for the provided record identifier
 * during the last data load.
 *
 * @param module The particular module containing the typeOrdinal below
 * @param typeOrdinal The type ordinal of the feature to be stored in the database
 * @param identifier The primary key of the record to be stored.
 * @param error Optional NSError reference to store information about any
 *        errors that occurred during the execution.
 * @return A flag indicating the detected operation or nil if an error occurred
 */
-(JDSRecordModificationType _Nullable) getModificationForRecord:(NSUInteger)identifier
                                              withOrdinal:(NSUInteger)typeOrdinal
                                                 inModule:(NSString* _Nonnull)module
                                                    error:(NSError *_Nullable __autoreleasing *_Nullable)error;

/*!
 * Retrieve the type of modification for all records of a given feature in a particular
 * module that occurred during the last data load.
 *
 * @param module The particular module containing the typeOrdinal below
 * @param typeOrdinal The type ordinal of the feature to be stored in the database
 * @param callback The block to be receiving the modification information per record
 * @param error Optional NSError reference to store information about any
 *        errors that occurred during the execution.
 * @return A flag indicating the detected operation or nil if an error occurred
 */
-(BOOL) getModificationForType:(NSUInteger)typeOrdinal
                      inModule:(NSString* _Nonnull)module
                      callback:(JDSRecordModificationInfoCallback _Nonnull)callback
                         error:(NSError *_Nullable __autoreleasing *_Nullable)error;

/*!
 * Retrieve the type of modification for all records of a given feature in a particular
 * module that occurred during the last data load.
 *
 * @param module The particular module containing the typeOrdinal below
 * @param callback The block to be receiving the modification information per part and type
 *        of modification (a part can have multiple types of modifications registered)
 * @param error Optional NSError reference to store information about any
 *        errors that occurred during the execution.
 * @return A flag indicating the detected operation or nil if an error occurred
 */
-(BOOL) getModifiedPartsInModule:(NSString* _Nonnull)module
                        callback:(JDSPartModificationInfoCallback _Nonnull)callback
                           error:(NSError *_Nullable __autoreleasing *_Nullable)error;

@end

/*! The public API for JDS Data Access.
 *
 * The JDS Data Access API provides a wrapper around the underlying database
 * connection. The primary purpose of this wrapper is to open the connection
 * to the JDS datastores. Upon successful connection, the user is free to either
 * use the JDS database interface provided in this module or to take the raw
 * SQLite3 database handle and perform the database operations using the native
 * APIs of SQLite3 or any other library that is able to wrap a provided database
 * connection handle.
 */
@interface JDSDataAccessAPI : NSObject

/*! Open the database connection for a given JDS Schema
 *
 * @param schema The JDSSchema instance to be used
 * @param stores The configuration of the associated stores
 * @param error Optional target for an error object.
 * @return The JDSConnection if the connection could be created, nil otherwise.
 */
+(JDSConnection* _Nullable) __attribute__((ns_returns_retained)) openConnection:(JDSSchema* _Nonnull)schema
                                withStores:(JDSStores* _Nonnull)stores
                                     error:(NSError *_Nullable __autoreleasing *_Nullable)error;

@end

