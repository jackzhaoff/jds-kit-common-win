///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSDataAccessAPI.h
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        Define the JDS status and error codes – These will be mapped
//        from the underlying C/C++ status codes.
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

FOUNDATION_EXPORT NSErrorDomain const JDSErrorDomain;

/*! Status codes returned from the JDS APIs */
typedef NS_ERROR_ENUM(JDSErrorDomain, JDSResult)
{
    /*! Success */
    JDSResultOK                  = JDS_OK,
    
    /*! Operation was cancelled by the user (typically during load operations only) */
    JDSResultCancelled           = JDS_CANCELLED,
    
    /*! Error cannot be more specific */
    JDSResultGeneral             = JDS_E_GENERAL,

    /*! Data within the currently processed loadset is invalid */
    JDSResultBadLoadsetContent   = JDS_E_LS_CONTENT,

    /*! The currently processed ZIP archive has format issues */
    JDSResultBadZipFormat        = JDS_E_ZIP_FORMAT,
    
    /*! Target store serial number is does not match expected baseline in delta loadset */
    JDSResultWrongBaseline       = JDS_E_LS_BASELINE,
    
    /*! Loadset contains data for unsupported module */
    JDSResultUnsupportedModule   = JDS_E_LS_MODULE,
    
    /*! No effectivity information found in store */
    JDSResultStoreNoEffectivity  = JDS_E_STORE_EFFECTIVITY,
    
    /*! Provided path to loadset bundle is no directory */
    JDSResultNoLoadsetDirectory  = JDS_E_LS_NODIR,
    
    /*! Provided path to loadset bundle is no zip archive */
    JDSResultNoLoadsetZip        = JDS_E_LS_NOZIP,
    
    /*! Requested entry not found in loadset bundle */
    JDSResultNoEntryInLoadset    = JDS_E_LS_NOENTRY,
    
    /*! An entry cannot be extracted from the processed ZIP archive */
    JDSResultZipExtractFailed    = JDS_E_ZIP_EXTRACT,
    
    /*! Database access occurred in an unsupported sequence. Typically indicating an internal problem */
    JDSResultBadSequencing       = JDS_E_DB_SEQUENCE,
    
    /*! Schema does not contain the requested module */
    JDSResultModuleNotFound      = JDS_E_SCHEMA_MODULE,
    
    /*! The loadset is of an unsupported version */
    JDSResultWrongLoadsetVersion = JDS_E_LS_VERSION,
    
    /*! The validation failed for the current loadset */
    JDSResultLoadsetInvalid      = JDS_E_LS_CHECKS,
    
    /*! No store available for requested module name */
    JDSResultNoStoreForModule    = JDS_E_STORE_MODULE
};


