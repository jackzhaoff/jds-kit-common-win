///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSExtent.hpp
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        Provide definition of the Extent class
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#ifdef __cplusplus

#include <intrin.h>
#include <bitset>
#include <memory>
#include <vector>
#include <limits.h>
#include <cstring>


#ifdef _WIN32
#define BITCOUNT(arg) __popcnt(arg)
#elif __APPLE__
#define BITCOUNT(arg) __builtin_popcountll(arg)
#endif

namespace jds
{

    template <size_t N> class BitSet
    {
        typedef BitSet<N>           bitset_type;
        
    public:
        typedef uint_fast64_t       bitset_content;
        static const size_t         bitset_shift_per_entry = 6;
        static const uint_fast64_t  bitset_mask_entry = UINTPTR_MAX;
        static const size_t         bitset_bits_per_entry = 64;
        static const uint_fast64_t  bitset_one = 1ull;

        static const size_t         bitset_array_size = (N >> bitset_shift_per_entry);

    protected:
        bitset_content              m_buffer[ bitset_array_size ];
        
    public:
        BitSet() {
            reset();
        }
        
        BitSet(const bitset_type& other) {
            memcpy(m_buffer, other.m_buffer, sizeof(m_buffer));
        }
        
        BitSet& operator=(const BitSet& other) {
            memcpy(m_buffer, other.m_buffer, sizeof(m_buffer));
            
            return (*this);
        }
        
        bool operator ==(const BitSet& other) const {
            return (0 == memcmp(m_buffer, other.m_buffer, sizeof(m_buffer)));
        }
        
        /**!
         * Invert all bits set in the bitset
         */
        inline void flip() {
            for (size_t i = 0; i < bitset_array_size; i++) {
                m_buffer[i] = ~m_buffer[i];
            }
        }
        
        /**!
         * Return allocated size of bitset (<= provided maximum size)
         */
        static inline size_t size() {
            return N;
        }
        
        size_t count() const {

            size_t result = 0;
            bitset_content* iterator = (bitset_content*) m_buffer;
            
            for (size_t i = 0; i < bitset_array_size; i++) {
                result += BITCOUNT( *iterator++ );
            }

            return result;
        }
        
        /**!
         * Return true if any of the bits in the bitset is set to true
         */
        bool any() const
        {
            bitset_content* iterator = (bitset_content*) m_buffer;
            
            for (size_t i = 0; i < bitset_array_size; i++) {
                if (*iterator++ != 0) {
                    return true;
                }
            }

            return false;
        }

        /**!
         * Return true if none of the bits in the bitset is set to true
         */
        inline bool none() const {
            return !any();
        }
        
        inline bool test(size_t index) const
        {
            return ((m_buffer[index >> bitset_shift_per_entry] >> (index & (bitset_bits_per_entry - 1))) & bitset_one);
        }
        
        inline void set(size_t index, bool value)
        {
            if (value) {
                m_buffer[index >> bitset_shift_per_entry] |= (bitset_one << (index & (bitset_bits_per_entry - 1)));
            }
            else {
                m_buffer[index >> bitset_shift_per_entry] &= ~(bitset_one << (index & (bitset_bits_per_entry - 1)));
            }
        }
        
        inline size_t find(size_t index, size_t endIndex, bool flag) const
        {
            size_t bucketIndex = index >> bitset_shift_per_entry;
            
            while (index < endIndex) {

                // If the current byte is either full or empty (depends on search),
                // we can skip the whole byte at once
                if (m_buffer[ bucketIndex ] == (flag ? 0x00 : bitset_mask_entry)) {
                    index = (bucketIndex + 1) << bitset_shift_per_entry;
                }
                else {
                    for (size_t subIndex = index & (bitset_bits_per_entry - 1);
                         (subIndex < bitset_bits_per_entry) &&
                         ((subIndex + (bucketIndex << bitset_shift_per_entry)) < endIndex);
                         subIndex++) {
                        
                        // Otherwise, bitwise search
                        if (((m_buffer[ bucketIndex ] >> subIndex) & bitset_one) == flag) {
                            return index;
                        }
                    
                        index++;
                    }
                }
                
                bucketIndex++;
            }
            
            return std::min(endIndex, index);
        }
        
        inline void fill(size_t& index, size_t count, bool flag)
        {
            while (((index & (bitset_bits_per_entry - 1)) != 0) && (count > 0)) {
                set(index++, flag);
                count--;
            }

            size_t tIndex = index >> bitset_shift_per_entry;
            bitset_content value = (flag ? bitset_mask_entry : 0x00);
            
            while ((count > (bitset_bits_per_entry - 1)) && (tIndex < bitset_array_size)) {
                m_buffer[ tIndex++ ] = value;
                count -= bitset_bits_per_entry;
                index += bitset_bits_per_entry;
            }
            
            while (count > 0) {
                set(index++, flag);
                count--;
            }
        }
        
        inline void set_byte(size_t& index, unsigned char byte)
        {
            static unsigned char lookup[16] = {
                0x0, 0x8, 0x4, 0xc, 0x2, 0xa, 0x6, 0xe,
                0x1, 0x9, 0x5, 0xd, 0x3, 0xb, 0x7, 0xf, };

            // Reverse the top and bottom nibble then swap them.
            bitset_content inverse = (bitset_content) (lookup[byte & 0b1111] << 4) | lookup[ byte >> 4];

            // Unaligned byte, need to do two updates
            size_t offset = index >> bitset_shift_per_entry;
            
            if (offset < bitset_array_size) {
                m_buffer[ offset ] = (m_buffer[ offset ] & (bitset_mask_entry >> (bitset_bits_per_entry - (index & (bitset_bits_per_entry - 1)))))
                                     | (inverse << (index & (bitset_bits_per_entry - 1)));
            }
            
            if ((index & (bitset_bits_per_entry - 1) + 8) > bitset_bits_per_entry) {
                offset++;
                
                if (offset < bitset_array_size) {
                    m_buffer[ offset ] = (m_buffer[ offset ] & (bitset_mask_entry << (bitset_bits_per_entry - (index & (bitset_bits_per_entry - 1)))))
                                        | (inverse >> (bitset_bits_per_entry - (index & (bitset_bits_per_entry - 1))));
                }
            }
            
            index = std::min(N, index + 8);
        }
        
        /**!
         * Reset the content and size of the array to zero
         */
        void reset()
        {
            std::memset(m_buffer, 0, sizeof(m_buffer));
        }
        
        
        bitset_type& operator&=( const bitset_type& other )
        {
            for (size_t i = 0; i < bitset_array_size; i++) {
                m_buffer[i] &= other.m_buffer[i];
            }

            return (*this);
        }
        
        bitset_type& operator|=( const bitset_type& other )
        {
            for (size_t i = 0; i < bitset_array_size; i++) {
                m_buffer[i] |= other.m_buffer[i];
            }
            
            return (*this);
        }
        
        bitset_type& operator^=( const bitset_type& other )
        {
            for (size_t i = 0; i < bitset_array_size; i++) {
                m_buffer[i] ^= other.m_buffer[i];
            }

            return (*this);
        }

        bitset_type operator~() const
        {
            bitset_type result(*this);
            result.flip();
            return result;
        }
    };
    
    /*!
     * The Extent is a compact representation of record identifiers. Every identifier
     * is mapped to a bit-string. As an Extent only stores a limited number of bits, the
     * virtually endless string is broken into segments of equal length and each segment
     * is numbered consecutively. With the segment number, the original identifier can be
     * calculated from the position of the bit in the bit-string of the index, offset by the
     * position of the Extent in the sequence of wrapped bit-strings.
     */
    class Extent
    {
    public:
        /*!
         * Size of a row in the bitset in bits. Aligned with the page-size
         * of the underlying database which is typically 4096.
         */
        static const int BITSET_SIZE = (1 << 14);
        
    public:
        typedef jds::BitSet<BITSET_SIZE>        extent_bits;
        typedef std::shared_ptr<extent_bits>    extent_bits_p;
        
    public:
        Extent();

        Extent(unsigned int typeOrdinal, unsigned long row);

        Extent(const Extent& other);
        
        Extent& operator =(const Extent& other);
        
        ~Extent();
        
    public:
        void clear();
        bool none() const;
        bool any() const;

        size_t count() const;
        
        Extent& operator&=( const Extent& other );
        Extent& operator|=( const Extent& other );
        Extent& operator^=( const Extent& other );
        Extent  operator~() const;

        Extent& flip();
        
        int next(int column) const;
        
        void reset(unsigned int typeOrdinal, unsigned long row);
        
        void set_at(size_t index, bool flag);
        bool get_at(size_t index);

        inline unsigned long toIdentifier(unsigned int column) {
            return toIdentifier(m_rowIndex, column);
        }

        static inline unsigned long toIdentifier(unsigned long row, unsigned int column) {
            return (unsigned long) Extent::BITSET_SIZE * row + column;
        }
        
        static inline unsigned long rowIndex(unsigned long identifier) {
            return (unsigned long) (identifier / (unsigned long) BITSET_SIZE);
        }
        
        static inline unsigned int columnIndex(unsigned long identifier) {
            return (unsigned int) (identifier - (BITSET_SIZE * rowIndex(identifier)));
        }
        
        inline unsigned int typeOrdinal() const { return m_typeOrdinal; }
        inline unsigned long row() const { return m_rowIndex; }

    protected:
        Extent(unsigned int typeOrdinal, unsigned long row, extent_bits bits);

    protected:
        volatile unsigned int                   m_typeOrdinal;
        volatile unsigned long                  m_rowIndex;
        
        extent_bits                             m_bits;
        
    public:
        std::vector<char> toBytes() const;

        void load(unsigned int typeOrdinal,
                  unsigned long rowIndex,
                  const std::vector<char> bytes);

        static Extent loadExtent(unsigned int typeOrdinal,
                                 unsigned long rowIndex,
                                 const std::vector<char> bytes);
    };
}

#endif

