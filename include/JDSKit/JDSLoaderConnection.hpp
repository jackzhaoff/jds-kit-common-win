///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSLoaderConnection.hpp
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        Provide an asynchronous API for SQLite. The class manages the
//        worker thread by itself. All operations are queued and executed
//        asynchronously unless they are not explicitly synched.
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#ifdef __cplusplus

#include <string>

#include <mutex>
#include <functional>
#include <thread>
#include <condition_variable>
#include <queue>
#include <stack>
#include <map>

#include "JDSConnection.hpp"
#include "JDSTypes.hpp"

namespace jds
{
    typedef unsigned int    TypeOrdinal;
    typedef unsigned long   Identifier;

    class TokenSource
    {
    public:
        virtual ~TokenSource() {}
        
    public:
        virtual bool getToken(bool& target) = 0;
        
        virtual bool getToken(char& target) = 0;
        virtual bool getToken(int& target) = 0;
        virtual bool getToken(long& target) = 0;
        
        virtual bool getToken(double& target) = 0;
        
        virtual bool getToken(std::vector<char>& target) = 0;
        virtual bool getToken(std::string& target) = 0;
        
        virtual bool getToken(jds::Geometry& target) = 0;
        
        virtual bool getToken(jds::Date& target) = 0;
        virtual bool getToken(jds::Time& target) = 0;
        virtual bool getToken(jds::DateTime& target) = 0;
        virtual bool getToken(jds::Numeric& target) = 0;
        
        virtual void skipToken() = 0;
    };
    
    class LoadsetStructure : public TokenSource
    {
    public:
        virtual ~LoadsetStructure() {}

    public:
        virtual bool getToken(bool& target) = 0;
        
        virtual bool getToken(char& target) = 0;
        virtual bool getToken(int& target) = 0;
        virtual bool getToken(long& target) = 0;
        
        virtual bool getToken(double& target) = 0;
        
        virtual bool getToken(std::vector<char>& target) = 0;
        virtual bool getToken(std::string& target) = 0;
        
        virtual bool getToken(jds::Geometry& target) = 0;
        
        virtual bool getToken(jds::Date& target) = 0;
        virtual bool getToken(jds::Time& target) = 0;
        virtual bool getToken(jds::DateTime& target) = 0;
        virtual bool getToken(jds::Numeric& target) = 0;
        
        virtual void skipToken() = 0;
    };
    
    class LoadsetRecord : public TokenSource
    {
    public:
        virtual ~LoadsetRecord() {}
        
    public:
        virtual bool getToken(bool& target) = 0;
        
        virtual bool getToken(char& target) = 0;
        virtual bool getToken(int& target) = 0;
        virtual bool getToken(long& target) = 0;
        
        virtual bool getToken(double& target) = 0;
        
        virtual bool getToken(std::vector<char>& target) = 0;
        virtual bool getToken(std::string& target) = 0;
        
        virtual bool getToken(jds::Geometry& target) = 0;
        
        virtual bool getToken(jds::Date& target) = 0;
        virtual bool getToken(jds::Time& target) = 0;
        virtual bool getToken(jds::DateTime& target) = 0;
        virtual bool getToken(jds::Numeric& target) = 0;
        
        virtual void skipToken() = 0;

        virtual bool getToken(std::shared_ptr<jds::LoadsetStructure>& target) = 0;
    };

    #undef DELETE
    
    enum StatementType {
        NONE,
        SQL,
        DELETE,
        STORE,
    };

    class LoaderConnection;
    
    class LoaderBinder
    {
    public:
        LoaderBinder(DBBinder& binder) : m_binder(binder) {}
        
    public:
        template <typename T> LoaderBinder& bind(std::shared_ptr<TokenSource> record)
        {
            T holder;
            
            return bind<T>(record, holder);
        }
        
        template <typename T> LoaderBinder& bind(std::shared_ptr<TokenSource> record, T& holder)
        {
            if (record->getToken(holder)) {
                bind(holder);
            }
            else {
                null();
            }
            
            return *this;
        }
        
        LoaderBinder& null();
        
        LoaderBinder& bind(const double value);
        
        LoaderBinder& bind(long value);
        LoaderBinder& bind(unsigned long value);
        
        LoaderBinder& bind(int value);
        LoaderBinder& bind(unsigned int value);
        
        LoaderBinder& bind(char value);
        LoaderBinder& bind(unsigned char value);
        
        LoaderBinder& bind(const std::string& value);
        LoaderBinder& bind(const std::vector<char>& value);

        LoaderBinder& bind(const jds::Geometry& value);
        LoaderBinder& bind(const jds::Bounds& value);
        
        LoaderBinder& bind(const jds::Date& value);
        LoaderBinder& bind(const jds::Time& value);
        LoaderBinder& bind(const jds::DateTime& value);
        LoaderBinder& bind(const jds::Numeric& value);
        
        LoaderBinder& bind(bool value);

    protected:
        DBBinder&           m_binder;
    };

    class LoaderRow
    {
    public:
        LoaderRow(DBReader& reader);
        
        LoaderRow(const LoaderRow& other);
        
        bool null(unsigned int i);
        const std::vector<char> blob(unsigned int i);
        double real(unsigned int i);
        int64_t int64(unsigned int i);
        int32_t int32(unsigned int i);
        const char *cstr(unsigned int i);
        std::string text(unsigned int i);
        
    protected:
        DBReader&   m_reader;
    };
    
    class LoaderStatement;
    
    typedef std::shared_ptr<DBStatement>                DBStatementPtr;
    typedef std::shared_ptr<LoaderStatement>            LoaderStatementPtr;
    typedef std::function<void(LoaderBinder& binder)>   BinderCallback;
    typedef std::function<bool(LoaderRow& binder)>      RowCallback;
    typedef std::function<void(bool complete)>          CompletionCallback;
    typedef std::pair<TypeOrdinal, StatementType>       CacheKeyType;
    
    /*!
     * This class wraps an actual execution into the database that will
     * be stored in the queue to be picked up by the executor.
     */
    class LoaderStatement
    {
    public:
        LoaderStatement(StatementType type = StatementType::NONE,
                        CompletionCallback completedCallback = [](bool) {});

        LoaderStatement(StatementType statementType,
                        TypeOrdinal typeOrdinal,
                        BinderCallback callback,
                        RowCallback rowCallback,
                        bool synchronized = false);
        
        LoaderStatement(const std::string sql,
                        BinderCallback bindCallback,
                        RowCallback rowCallback,
                        bool synchronized = false);

        LoaderStatement(const LoaderStatement& other);

        LoaderStatement& operator=(const LoaderStatement& other);
        
        DBStatementPtr prepare(LoaderConnection& connection);

        void finishStatement(DBStatementPtr dbStmt, LoaderConnection& connection);
        
    public:
        inline TypeOrdinal typeOrdinal() const { return m_typeOrdinal; }
        inline StatementType statementType() const { return m_statementType; }
        inline bool synchronous() const { return m_synchronized; }
        
        /*!
         * Execute the statement against the provided connection using the provided prepared
         * statement.
         *
         * @param dbStmt Reference to the prepared statement to be used
         * @param connection The database connection to execute the statement
         * @return @c true if the execution was successful, @c false if an error occurred.
         */
        bool execute(DBStatementPtr dbStmt, LoaderConnection& connection);
     
    protected:
        std::string             m_sql;

        TypeOrdinal             m_typeOrdinal;
        StatementType           m_statementType;
        
        BinderCallback          m_bindCallback;
        RowCallback             m_rowCallback;
        
        bool                    m_synchronized;
        
        //! Flag to indicate that the execution ran into an error
        volatile bool           m_hasException;
    };
    
    class LoaderConnection
    {
        friend class LoaderStatement;
        
    public:
        LoaderConnection();

        LoaderConnection(DBConnection& db);

        LoaderConnection(const std::string& filename);
        LoaderConnection(const char *filename);
        
        ~LoaderConnection();

        void shutdown();
        
    public:
    
        void executeSync(std::string sql,
                         BinderCallback bindCallback,
                         RowCallback rowCallback);

        void executeSync(std::string sql,
                         BinderCallback bindCallback);

        void executeSync(std::string sql,
                         RowCallback rowCallback);

        void executeSync(std::string sql);
        
        void execute(std::string sql);

        void execute(std::string sql,
                     BinderCallback bindCallback,
                     RowCallback rowCallback);

        void execute(std::string sql,
                     BinderCallback bindCallback);

        void execute(std::string sql,
                     RowCallback rowCallback);

        void execute(StatementType op,
                     TypeOrdinal typeOrdinal,
                     const char* sql);

        void execute(StatementType op,
                     TypeOrdinal typeOrdinal,
                     const std::string sql);

        void execute(StatementType op,
                     TypeOrdinal typeOrdinal,
                     const char* sql,
                     BinderCallback bindCallback,
                     RowCallback rowCallback = [](LoaderRow&) -> bool { return true; });

        void execute(StatementType op,
                     TypeOrdinal typeOrdinal,
                     const std::string sql,
                     BinderCallback bindCallback,
                     RowCallback rowCallback = [](LoaderRow&) -> bool { return true; });

        void execute(StatementType op,
                     TypeOrdinal typeOrdinal,
                     const char* sql,
                     RowCallback rowCallback);

        void execute(StatementType op,
                     TypeOrdinal typeOrdinal,
                     const std::string sql,
                     RowCallback rowCallback);

        /*!
         * Terminate any running operation and clear any outstanding statements from the queue
         */
        void terminate();
        
        /*!
         * Record an exception raised during the execution of a statement in the worker thread.
         * With that, the execution of any further statements will be terminated and an error returned
         * to the controlling thread.
         *
         * @param e The exception to be recorded.
         */
        void recordException(JDSException& e);
        
    protected:
        /*!
         * This it the threads worker loop that picks up statements
         * from the statement queue and executes
         */
        static void executor(LoaderConnection& connection);
        
        /*!
         * Fetch a statement from the queue
         */
        LoaderStatement dequeue();

        /*!
         * Place a statement in the queue
         */
        void enqueue(LoaderStatement statement);

        /*!
         * Check the state of the queue and eventually notify any waiting threads
         */
        void notifyWriters();
        
        /*!
         * This will remove any entries from the queue to allow faster shutdown in
         * case of an error
         */
        void clearQueue();
        
        /*!
         * Retrieve a prepared statement from the cache. If needed, the
         * stored SQL event is then prepared in the underlying database
         */
        DBStatementPtr getStatement(const std::string sql);
        
        /*!
         * Retrieve a prepared statement from the cache. If needed, the
         * stored SQL event is then prepared in the underlying database
         */
        DBStatementPtr getStatement(StatementType op,
                                    TypeOrdinal typeOrdinal);
        
        /*!
         * Store a database statement in the internal cache
         */
        void cacheStatement(StatementType op,
                            TypeOrdinal typeOrdinal,
                            DBStatementPtr dbStmt);

        /*!
         * Store a database statement in the internal cache
         */
        void cacheStatement(const std::string sql,
                            DBStatementPtr dbStmt);

        /*!
         * Store a SQL command in the statement cache for later execution
         */
        void registerStatement(StatementType op,
                               TypeOrdinal typeOrdinal,
                               const char* sql);

        void checkTermination();
        
        void doSync();
        
    protected:
        //! This mutex protects the database from concurrent access. The executor will use this to guard the
        //  execution of a single statement. This is expecially important as a statement that is executed, may
        //  enqueue new statements for execution during a synchronous execution.
        std::mutex                                                  m_dbLock;
        
        // This informs the queue managers that any additional executions are coming from a running execution
        volatile bool                                               m_nestedExecution;
        
        //! The underlying database connection
        DBConnection                                                m_db;

        //! This is the thread that will execute the statements
        std::thread                                                 m_consumer;

        //! This mutex protects the queue for concurrent access
        std::mutex                                                  m_queueLock;

        //! This mutex protects the queue for concurrent access
        std::mutex                                                  m_syncLock;

        //! This mutex controls access to the API to allow controlled shutdown of the object
        std::mutex                                                  m_executionLock;

        //! This mutex controls access to the API to allow controlled shutdown of the object
        std::mutex                                                  m_apiLock;

        //! This mutex controls access to information about errors and cancellations
        std::mutex                                                  m_terminationStatusLock;

        //! The statement queue
        std::queue<LoaderStatement>                                 m_queue;

        //! This mutext protects the SQL statement cache
        std::mutex                                                  m_sqlLock;
        std::map<CacheKeyType, std::string>                         m_sqlCache;

        //! Flag to indicate that the connection was cancelled and that any running
        //  operation should terminate as soon as possible.
        volatile bool                                               m_cancelled;

        //! Flag to indicate that the connection is shutting down
        volatile bool                                               m_shutdown;

        //! Flag to indicate that the execution ran into an error
        volatile bool                                               m_hasException;
        
        //! The recorded exception
        JDSException                                                m_exception;
        
        //! Store the maximum number of statements stored in the LoaderConnection before adding would get blocked
        const int                                                   m_capacity;
        
        std::condition_variable                                     m_not_full;
        std::condition_variable                                     m_not_empty;
        std::condition_variable                                     m_empty;

        std::condition_variable                                     m_sync;

        //! This mutext protects the prepared statements cache for direct sql statements
        std::mutex                                                  m_sqlStatementLock;
        
        //! The prepared statements cache
        std::map<const std::string, std::stack<DBStatementPtr>>     m_sqlStatementCache;

        
        //! This mutext protects the prepared statements cache
        std::mutex                                                  m_cacheLock;
        
        //! The prepared statements cache
        std::map<CacheKeyType, std::stack<DBStatementPtr>>          m_cache;        
    };
}

#endif

