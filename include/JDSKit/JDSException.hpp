///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSException.hpp
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        Central exception class for the JDS APIs. All functions will
//        throw either a JDSException or a DBException upon an error.
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#ifdef __cplusplus

#include <JDSKit/JDSStatus.h>

#include <string>

namespace jds
{

    class JDSException : public std::exception
    {
    protected:
        std::string     m_what;

        unsigned int    m_code;
                
    public:
        JDSException(const JDSException& other) :
            m_what(other.m_what),
            m_code(other.m_code)
        {
        }
        
        explicit JDSException(unsigned int code,
                              const char* message) :
            m_what(message),
            m_code(code)
        {
        }

        explicit JDSException(unsigned int code,
                              const std::string message) :
            m_what(message),
            m_code(code)
        {
        }

        explicit JDSException(unsigned int code,
                              const char* message,
                              std::exception& origin) :
            m_what(message),
            m_code(code)
        {
        }

        
        explicit JDSException(unsigned int code,
                              const char* message,
                              const JDSException& origin) :
            m_what(message),
            m_code(code)
        {
        }

        explicit JDSException(unsigned int code,
                              std::string message,
                              std::exception& origin) :
            m_what(message),
            m_code(code)
        {
        }

    public:
        /*! Destructor.
         * Virtual to allow for subclassing.
         */
        virtual ~JDSException() throw () {}

        /*!
         * Returns a pointer to the (constant) error description.
         *  @return A pointer to a const char*. The underlying memory
         *          is in posession of the Exception object. Callers must
         *          not attempt to free the memory.
         */
        virtual const char* what() const throw () {
            return m_what.c_str();
        }
        
        /*!
         * Return the error code that was recorded for this exception.
         *
         * @return JDSStatus error code.
         */
        inline int code() const {
            return m_code;
        }

    };
}

#endif

