///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSSchema.h
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        Define the C++ JDS Schema Interface
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#ifdef __cplusplus

#include <string>
#include <memory>
#include <map>
#include <vector>

#include "JDSVersionInfo.hpp"
#include "JDSConnection.hpp"
#include "JDSExtent.hpp"
#include "JDSCommons.hpp"

#include <JDSKit/JDSLoaderConnection.hpp>

namespace jds
{
    struct TableColumnSpec
    {
        const std::string   name;
        const std::string   type;
        const bool          nonnull;
        const bool          primary;
    };

    struct IndexColumnSpec
    {
        const std::string   name;
        const bool          descending;
    };

    /*!
     * This is the virtual base class for all APIs that allows to check the
     * version of an API so that potential incompatibilities can be foreseen
     * and crashes can be avoided.
     */
    class APIVersion
    {
    public:
        virtual ~APIVersion() {}
        
        /*!
         * Retrieve the API version.
         *
         * @return the version of the API as VersionInfo instance.
         */
        virtual const VersionInfo& api_version() const = 0;
    };
    
    /*!
     * Defines the public API of a JDS Module
     */
    class Module : public APIVersion
    {
    protected:
        const VersionInfo   m_version;

        Module(const VersionInfo& version) : m_version(version) {}

    public:
        /*!
         * The version of the schema module
         */
        const VersionInfo& version() const { return m_version; }
        
        /*!
         * The name of the schema module
         */
        const std::string name() const { return m_version.name(); }

    public:
        /*!
         * Create the database schema in an empty database instance. The
         * schema shall not contain any indexes that are created or dropped
         * by the createIndexesAndConstraints and dropIndexesAndConstraints
         * methods.
         *
         * @param context The database connection
         * @throws JDSException if the operation failed.
         */
        virtual void createSchema(LoaderConnection& context) const = 0;
        
        /*!
         * Create any indexes and constraints for the database. This method is only
         * called for database creation and before loading full loadsets.
         *
         * @param context The database connection
         * @throws JDSException if the operation failed.
         */
        virtual void createIndexesAndConstraints(LoaderConnection& context) const = 0;
        
        /*!
         * Drop any indexes and constraints from the database. This method is only
         * called before full loadsets are loaded.
         *
         * @param context The database connection
         * @throws JDSException if the operation failed.
         */
        virtual void dropIndexesAndConstraints(LoaderConnection& context) const = 0;

        /*!
         * Update any indexes with the data stored in the features. This method is only
         * called after loading full loadsets.
         *
         * @param context The database connection
         * @throws JDSException if the operation failed.
         */
        virtual void populateIndexes(LoaderConnection& context) const = 0;

        /*!
         * Update any indexes with the data stored in the features referenced by the provided Extent.
         * This method is called multiple times during the load of an incremental loadset.
         *
         * @param context The database connection
         * @param extent The Extent instance that stores the record identifiers to update.
         *               The Extent will also supply the type ordinal of the updated feature.
         * @throws JDSException if the operation failed.
         *
         * @deprecated This function is no longer used since API version 1.2.0 and will only
         *             stay here for API compatibility reason
         */
        #ifdef __APPLE__
        __AVAILABILITY_INTERNAL_DEPRECATED
        #endif
        virtual void updateIndexes(LoaderConnection& context, const Extent& extent) const = 0;

        /*!
         * Populate a newly created database with any non-feature values. This method is called
         * once after the database was created.
         *
         * @param context The database connection
         * @throws JDSException if the operation failed.
         */
        virtual void populateData(LoaderConnection& context) const = 0;
        
        /*!
         * Check if a given type ordinal is supported by this schema
         *
         * @param typeOrdinal The unique type ordinal of the feature type to check.
         * @throws JDSException if the operation failed.
         */
        virtual bool supportsType(const unsigned int typeOrdinal) const = 0;
        
        /*!
         * Delete any records that are referenced in the provided Extent instance.
         *
         * @param context The database connection
         * @param extent The Extent instance that stores the record identifiers to be
         *               deleted. The Extent will also supply the type ordinal of the feature
         *               this deletion applies to.
         * @throws JDSException if the operation failed.
         */
        virtual void removeRecords(LoaderConnection& context, const Extent& extent) const = 0;

        /*!
         * Store a record in the database. This method is called for new and updated records.
         *
         * @param context The database connection
         * @param typeOrdinal The type ordinal of the feature to be stored in the database
         * @param identifier The primary key of the record to be stored.
         * @param record Reference to the token source that provides the values for the feature
         *               attributes.
         * @throws JDSException if the operation failed.
         */
        virtual void storeRecord(LoaderConnection& context,
                                 const TypeOrdinal typeOrdinal,
                                 const unsigned long identifier,
                                 std::shared_ptr<LoadsetRecord> record) const = 0;

        /*!
         * Verify the database schema of the provided store
         *
         * @param db The database connection to validate.
         * @param scope the complexity of the checks.
         * @return The state of the store
         * @throws JDSException if the operation failed.
         */
        virtual StoreState validateModuleSchema(DBConnection& db,
                                                const ValidationScope scope) const = 0;
        
        /*!
         * Validate the basic schema of the store. This includes mainly the
         * core JDS* tables defined for the JDS metadata.
         *
         * @param db The database connection to validate.
         * @param version The version to validate the database against.
         * @return @c true if the validation was successful. @c false otherwise.
         * @throws JDSException if the operation failed.
         */
        bool validateCoreSchema(DBConnection&           db,
                                const VersionInfo&      version) const;
        
        /*!
         * Validate the schema of a table.
         *
         * @param db The database connection to validate.
         * @param table The name of the table to validate.
         * @param columns Specification of the tables columns.
         * @return @c true if the validation was successful. @c false otherwise.
         * @throws JDSException if the operation failed.
         */
        bool validateTable(DBConnection&                db,
                           const char*                  table,
                           std::vector<TableColumnSpec> columns) const;

        /*!
         * Validate an index in the database.
         *
         * @param db The database connection to validate.
         * @param table The name of the table to validate.
         * @param index The name of the index to validate.
         * @param unique If the index is supposed to be unique
         * @param columns Specification of the index columns.
         * @return @c true if the validation was successful. @c false otherwise.
         * @throws JDSException if the operation failed.
         */
        bool validateIndex(DBConnection&                db,
                           const char*                  table,
                           const char*                  index,
                           const bool                   unique,
                           std::vector<IndexColumnSpec> columns) const;
        
        /*!
         * Validate an r-tree index in the database.
         *
         * @param db The database connection to validate.
         * @param index The name of the index to validate.
         * @param keyColumn Name of the key column in the index
         * @return @c true if the validation was successful. @c false otherwise.
         * @throws JDSException if the operation failed.
         */
        bool validateRTreeIndex(DBConnection&           db,
                                const char*             index,
                                const char*             keyColumn) const;

    };

    /*!
     * Define the JDS data schema
     */
    class Schema : public APIVersion
    {
    protected:
        const VersionInfo                                       m_version;

        std::map<const std::string, std::shared_ptr<Module>>    m_modules;
        
        Schema(const VersionInfo& version) : m_version(version) {}

        void addModule(std::shared_ptr<Module> module)
        {
            m_modules[ module->name() ] = module;
        }
        
    public:
        /*!
         * The name of the schema
         */
        const std::string name() const { return m_version.name(); }
        
        /*!
         * The version of the schema
         */
        const VersionInfo version() const { return m_version; }
        
        /*!
         * All modules contained in the schema
         */
        const std::vector<std::shared_ptr<Module>> modules() const;

        /*!
         * Check if a given module with a particular name and version is contained
         * in the schema.
         *
         * @param version The version information to search for.
         * @return @c true if the module is contained in the schema, @c false otherwise.
         * @throws JDSException if the execution failed.
         */
        bool containsModule(const jds::VersionInfo& version) const;
        
        /*!
         * Return a module with a particular name and version if it is contained
         * in the schema.
         *
         * @param version The version information to search for.
         * @return Pointer to the module if it was found.
         * @throws JDSException if the execution failed or the module was not found.
         */
        const std::shared_ptr<Module> getModule(const jds::VersionInfo& version) const;

    };
}

#endif

