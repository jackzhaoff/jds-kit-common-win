///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSKit.h
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        The framework umbrella header for JDSKit
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#import <Foundation/Foundation.h>

#import <JDSKit/JDSTypes.h>
#import <JDSKit/JDSStatus.h>
#import <JDSKit/JDSErrors.h>
#import <JDSKit/JDSStoreAPI.h>
#import <JDSKit/JDSLoaderAPI.h>
#import <JDSKit/JDSLoadsetAPI.h>
#import <JDSKit/JDSDataAccessAPI.h>

#import <JDSKit/JDSKit.hpp>

