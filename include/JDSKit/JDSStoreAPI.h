///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSStoreAPI.h
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        JDS Store API
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#import <JDSKit/JDSKit.h>
#import <JDSKit/JDSLoaderAPI.h>

/*!
 * @typedef JDSStoreState
 *
 * Enumeration of the different states a store could have.
 */
typedef NSString * const JDSStoreState NS_STRING_ENUM;

/*! The store is valid with regard to the selected validation scope */
FOUNDATION_EXPORT JDSStoreState _Nonnull JDSStoreStateValid;

/*! The store is empty */
FOUNDATION_EXPORT JDSStoreState _Nonnull JDSStoreStateEmpty;

/*! The data store contains a data schema with a different version */
FOUNDATION_EXPORT JDSStoreState _Nonnull JDSStoreStateWrongSchema;

/*! The data store is not valid as it has schema deviations. */
FOUNDATION_EXPORT JDSStoreState _Nonnull JDSStoreStateInvalid;

/*! The data store does not exist. */
FOUNDATION_EXPORT JDSStoreState _Nonnull JDSStoreStateNotAvailable;

/*!
 * Declare the amount of information to be verified with a call to one
 * of the validation functions.
 */
typedef NS_ENUM(NSInteger, JDSValidationScope)
{
    /*!
     * Validate only the core data schema. This is the fastest check
     */
    JDSValidationScopeFast       = 1,
    
    /*!
     * Validate the complete data schema. This is typically a slower
     * operation than only the check of the core data schema
     */
    JDSValidationScopeComplete   = 3
};

/*!
 * @typedef JDSSourceType
 *
 * Type of data source used for last data load
 */
typedef NSString * const JDSSourceType NS_STRING_ENUM;

/*! The store is valid with regard to the selected validation scope */
FOUNDATION_EXPORT JDSSourceType _Nonnull JDSSourceTypeFull;

/*! The store is empty */
FOUNDATION_EXPORT JDSSourceType _Nonnull JDSSourceTypeIncremental;

/*!
 * @interface JDSStore
 *
 * Define a single data store. Every store is associated with one module
 * of a data schema. Multiple JDSStore definitions are grouped together
 * with a JDSStores instance that maps each module of a data schema to a
 * store definition.
 *
 * The store is identified by the name of the associated module.
 * Every store requires the path to the directory where the store data is
 * to be saved.
 */
@interface JDSStore : NSObject

@property (readonly, copy) NSString* _Nonnull path;

@property (readonly, copy) NSString* _Nonnull moduleName;

/*!
 * Static initializer that creates a JDSStore instance from the provided
 * parameters.
 *
 * @param module The name of the module this store is associated with. The
 *        module name is case sensitive.
 * @param path The path to the directory where the store data should be saved.
 * @param cipher An optional cipher password to be used to encrypt and decrypt
 *        the data store.
 * @return A populated JDSStore instance.
 */
+(JDSStore*_Nonnull) createStoreForModule:(NSString*_Nonnull)module
                                   toPath:(NSString*_Nonnull)path
                               withCipher:(NSString*_Nullable)cipher;

/*!
 * Load the effectivity from the store. Since the effectivity is stored in a
 * schema independent manner, this can be performed without actually knowing
 * the actual module that is stored in this store.
 *
 * @param error Will receive detailed error information in case the load
 *        fails for any reason.
 * @return The JDSEffectivity object with the loaded information. If an error
 *         occurred, a nil value is returned. In that case the error argument
 *         would contain additional information.
 */
-(JDSEffectivity* _Nullable) loadEffectivity:(NSError*_Nullable*_Nullable)error;

/*!
 * Load the version information from the store. Since the version is stored in a
 * schema independent manner, this can be performed without actually knowing
 * the actual module that is stored in this store.
 *
 * @param error Will receive detailed error information in case the load
 *        fails for any reason.
 * @return The JDSVersion object with the loaded information. If an error
 *         occurred, a nil value is returned. In that case the error argument
 *         would contain additional information.
 */
-(JDSVersion* _Nullable) loadVersion:(NSError*_Nullable*_Nullable)error;

/*!
 * Load the name of the parts loaded into the store.
 *
 * @param error Will receive detailed error information in case the load
 *        fails for any reason.
 * @return The NSArray object with the part names. If an error
 *         occurred, a nil value is returned. In that case the error argument
 *         would contain additional information.
 */
-(NSArray<NSString*>* _Nullable) loadParts:(NSError*_Nullable*_Nullable)error;

/*!
 * Load the serial number of the store.
 *
 * @param error Will receive detailed error information in case the load
 *        fails for any reason.
 * @return The NSString with the serial number or an empty string. If an error
 *         occurred, a nil value is returned. In that case the error argument
 *         would contain additional information.
 */
-(NSString* _Nullable) loadSerial:(NSError*_Nullable*_Nullable)error;


/*!
 * Retrieve source type of last data load from the store.
 *
 * @param error Will receive detailed error information in case the load
 *        fails for any reason.
 * @return The type of source data
 */
-(JDSSourceType _Nullable) loadSourceType:(NSError*_Nullable*_Nullable)error;

/*!
 * Validate this store. Depending on the selected validation scope,
 * only the core JDS data schema is checked, or the complete data schema
 * will be checked against the provided data schema.
 *
 * @param schema A data schema used for the thorough validation of the
 *        data store.
 * @param scope The JDSValidationScope selecting the complexity of the
 *        applied validations.
 * @param error Returns an NSError object with error details when the
 *        validation failed.
 * @return The store state
 */
-(JDSStoreState _Nullable) validateWithSchema:(JDSSchema* _Nonnull)schema
                                     andScope:(JDSValidationScope)scope
                                        error:(NSError*_Nullable*_Nullable)error;

/*!
 * Validate the provided store. With this call, only a fast validation
 * as selected by JDSValidationScopeFast is performed.
 *
 * @param error Returns an NSError object with error details when the
 *        validation failed.
 * @return The store state
 */
-(JDSStoreState _Nullable) validate:(NSError*_Nullable*_Nullable)error;

/*!
 * Move the content of the current store descriptor to another location
 * provided by the target store descriptor. Any data in the target store
 * will be overwritten.
 *
 * NOTE: This function does not reflect the used encryption ciphers. So
 *       moving data from a store with cipher to a store without cipher
 *       does not affect the encryption of the underlying store.
 *
 * @param store The store descriptor providing the target path
 * @param error Returns an NSError object with error details when the
 *        validation failed.
 * @return YES if the operation succeeded, NO otherwise.
 */
-(BOOL) moveToStore:(JDSStore* _Nonnull)store
              error:(NSError*_Nullable*_Nullable)error;

/*!
 * Replicate the content of the current store descriptor to another location
 * provided by the target store descriptor. Any data in the target store will be
 * overwritten.
 *
 * NOTE: This function does not reflect the used encryption ciphers. So
 *       moving data from a store with cipher to a store without cipher
 *       does not affect the encryption of the underlying store.
 *
 * @param store The store descriptor providing the target path
 * @param error Returns an NSError object with error details when the
 *        validation failed.
 * @return YES if the operation succeeded, NO otherwise.
 */
-(BOOL) copyToStore:(JDSStore* _Nonnull)store
              error:(NSError*_Nullable*_Nullable)error;

/*!
 * Remove all data in the current store.
 *
 * @param error Returns an NSError object with error details when the
 *        validation failed.
 * @return YES if the operation succeeded, NO otherwise.
 */
-(BOOL) clear:(NSError*_Nullable*_Nullable)error;


@end

/*!
 * @interface JDSStores
 *
 * Collect one ore more JDSStore objects to map the modules within a JDSSchema
 * to their corresponding storage locations and eventually supply the required
 * cryptographic key to encrypt and decrypt the data.
 */
@interface JDSStores : NSObject

/*!
 * Add the configuration for a store. Since a module can only be represented once per schema, the
 * same rule also applies for the module specific mapping. Any mapping added for a given module will
 * overwrite any preceding mapping found in the configuration.
 *
 * @param module The name of the mapped module. The parameter is case sensitive and must be the same as
 *              in the corresponding module.
 * @param path The filesystem path of the storage for the coirresponding storage file.
 * @param cipher An optional cryptographic cipher used for encrypting and decrypting the storage file.
 * @return The JDSStore created for this new entry.
 */
-(JDSStore*_Nonnull) addStoreForModule:(NSString*_Nonnull)module
                                toPath:(NSString*_Nonnull)path
                            withCipher:(NSString*_Nullable)cipher;

/*!
 * Validate this store. Depending on the selected validation scope,
 * only the core JDS data schema is checked, or the complete data schema
 * will be checked against the provided data schema.
 *
 * @param schema A data schema used for the thorough validation of the
 *        data store.
 * @param scope The JDSValidationScope selecting the complexity of the
 *        applied validations.
 * @param error Returns an NSError object with error details when the
 *        validation failed.
 * @return YES if the validation succeeded, NO if an error occurred.
 */
-(JDSStoreState _Nullable) validateWithSchema:(JDSSchema* _Nonnull)schema
                                     andScope:(JDSValidationScope)scope
                                        error:(NSError*_Nullable*_Nullable)error;

/*!
 * Validate the provided store. With this call, only a fast validation
 * as selected by JDSValidationScopeFast is performed.
 *
 * @param error Returns an NSError object with error details when the
 *        validation failed.
 * @return YES if the validation succeeded, NO otherwise.
 */
-(JDSStoreState _Nullable) validate:(NSError*_Nullable*_Nullable)error;

/*!
 * Process a loadset bundle and applies its content to a provided store
 * using a provided schema specification. If the loadset contains a full
 * load, then the associated store will be initialized with a new database,
 * in case of an incremental update, the associated store must contain the
 * matching baseline data, otherwise the data load will fail.
 *
 * @param loadsetPath The path to the loadset bundle which either can be a
 *        folder containing the loadset data, or a ZIP archive with the
 *        loadset bundle data.
 * @param schema The data schema to be used to apply the loadset. It will be
 *        verified against the loadset schema specification, and the schema
 *        of the store in case of an incremental update.
 * @param callback A callback that receives progress updates and allows the
 *        caller to terminate the load processed by returning false.
 * @param error Will receive detailed error information in case the load
 *        fails for any reason. Also a premature termination by the caller
 *        will be reported as an error (JDSStatusCancelled).
 * @return YES if the validation succeeded, NO if an error occurred.
 */
-(BOOL) loadBundle:(NSString*_Nonnull)loadsetPath
        withSchema:(JDSSchema*_Nonnull)schema
          callback:(JDSLoaderCallback _Nullable)callback
             error:(NSError*_Nullable*_Nullable)error;

/*!
 * Process a loadset bundle and applies its content to a provided store
 * using a provided schema specification. If the loadset contains a full
 * load, then the associated store will be initialized with a new database,
 * in case of an incremental update, the associated store must contain the
 * matching baseline data, otherwise the data load will fail.
 *
 * @param loadsetPath The path to the loadset bundle which either can be a
 *        folder containing the loadset data, or a ZIP archive with the
 *        loadset bundle data.
 * @param schema The data schema to be used to apply the loadset. It will be
 *        verified against the loadset schema specification, and the schema
 *        of the store in case of an incremental update.
 * @param memoryLimit Allows providing a rough upper limit for the size of
 *        records being held concurrently in memory during the load. This limit
 *        might temporally be exceeded and should only be considered a recommendation
 *        for the loader. If the value is 0 then no limit is applied.
 * @param callback A callback that receives progress updates and allows the
 *        caller to terminate the load processed by returning false.
 * @param error Will receive detailed error information in case the load
 *        fails for any reason. Also a premature termination by the caller
 *        will be reported as an error (JDSStatusCancelled).
 * @return YES if the validation succeeded, NO if an error occurred.
 */
-(BOOL) loadBundle:(NSString*_Nonnull)loadsetPath
        withSchema:(JDSSchema*_Nonnull)schema
    andMemoryLimit:(NSUInteger)memoryLimit
          callback:(JDSLoaderCallback _Nullable)callback
             error:(NSError*_Nullable*_Nullable)error;

@end


