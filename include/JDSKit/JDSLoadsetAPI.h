///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSLoadsetAPI.h
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        JDS Loadset API
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#import <JDSKit/JDSKit.h>
#import <JDSKit/JDSLoaderAPI.h>

/*!
 * @typedef JDSLoadsetType
 *
 * Describes the type of loadset (Full or Delta)
 */
typedef NS_ENUM(NSInteger, JDSLoadsetType)
{
    /*!
     * Full loadset
     */
    JDSLoadsetTypeFull          = 1,
    
    /*!
     * Incremental Loadset
     */
    JDSLoadsetTypeDelta         = 2
};

@interface JDSLoadset : NSObject

@property (readonly, nonatomic) NSString* _Nonnull part;

@property (readonly, atomic) JDSLoadsetType type;

@property (readonly, atomic) unsigned long recordCount;

@end

@interface JDSLoadsetBundle : NSObject

/*!
 * Open a loadset bundle
 *
 */
+(JDSLoadsetBundle* _Nullable) open:(NSString* _Nonnull)path
                              error:(NSError *_Nullable __autoreleasing *_Nullable )error;

@property (readonly, nonatomic) NSString* _Nonnull path;

@property (readonly, nonatomic) JDSVersion* _Nonnull version;

@property (readonly, nonatomic) NSString* _Nullable baselineSerial;

@property (readonly, nonatomic) NSString* _Nonnull serial;

@property (readonly, atomic) JDSLoadsetType type;

@property (readonly, nonatomic) JDSEffectivity* _Nonnull effectivity;

@property (readonly, atomic) unsigned long recordCount;

@property (readonly, nonatomic) NSArray<JDSVersion*>* _Nonnull compatibleVersions;

@property (readonly, nonatomic) NSArray<JDSLoadset*>* _Nonnull loadsets;

@property (readonly, nonatomic) NSDictionary<NSString*,NSString*>* _Nonnull additionalData;

/*!
 * Process a loadset bundle and applies its content to a provided store
 * using a provided schema specification. If the loadset contains a full
 * load, then the associated store will be initialized with a new database,
 * in case of an incremental update, the associated store must contain the
 * matching baseline data, otherwise the data load will fail.
 *
 * @param schema The data schema to be used to apply the loadset. It will be
 *        verified against the loadset schema specification, and the schema
 *        of the store in case of an incremental update.
 * @param storeConfigs The configuration of the stores that carry the data
 *        for each module in the schema. Since a loadset will always
 *        only contain data for one module, it would be sufficient to supply
 *        only a store configuration for that module.
 * @param callback A callback that receives progress updates and allows the
 *        caller to terminate the load processed by returning false.
 * @param error Will receive detailed error information in case the load
 *        fails for any reason. Also a premature termination by the caller
 *        will be reported as an error (JDSStatusCancelled).
 */
-(BOOL) loadBundleWithSchema:(JDSSchema*_Nonnull)schema
                   andStores:(JDSStores*_Nonnull)storeConfigs
                    callback:(JDSLoaderCallback _Nullable)callback
                       error:(NSError*_Nullable*_Nullable)error;

@end

