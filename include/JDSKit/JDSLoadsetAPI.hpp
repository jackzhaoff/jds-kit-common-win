///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSLoadsetAPI.h
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        Define the C++ JDS Loadset API
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#ifdef __cplusplus

#include <JDSKit/JDSKit.hpp>

#include <memory>
#include <string>
#include <map>

namespace jds
{
    class LoadsetBundle;
    
    /*!
     * Define the type of a loadset
     */
    enum LoadsetType
    {
        /*! A FULL Loadset */
        FULL,
        
        /*! An incremental DELTA Loadset */
        DELTA
    };
    
    typedef enum LoadsetType LoadsetType;
    
    /*!
     * Store the metadata for a loadset.
     */
    class LoadsetInfo
    {
    public:
        LoadsetInfo(const std::string part, const LoadsetType& type, size_t recordCount) :
            m_part(part),
            m_type(type),
            m_recordCount(recordCount)
        {}

        LoadsetInfo(const LoadsetInfo& other) :
            m_part(other.m_part),
            m_type(other.m_type),
            m_recordCount(other.m_recordCount)
        {}

        LoadsetInfo& operator =(const LoadsetInfo& other)
        {
            m_part = other.m_part;
            m_type = other.m_type;
            m_recordCount = other.m_recordCount;
            
            return *this;
        }

        bool operator ==(const LoadsetInfo& other) const
        {
            return (m_part == other.m_part)
                && (m_type == other.m_type)
                && (m_recordCount == other.m_recordCount);
        }

    protected:
        std::string     m_part;
        LoadsetType     m_type;
        size_t          m_recordCount;
        
    public:
        /*! The type of the loadset */
        const LoadsetType& type() const { return m_type; }

        /*! The part identifier of this loadset */
        const std::string& part() const { return m_part; }

        /*! The number of records contained in this loadset */
        const size_t recordCount() const { return m_recordCount; }
    };
    
    /*!
     * Store the metadata for a loadset bundle.
     */
    class LoadsetBundleInfo
    {
    protected:
        LoadsetBundleInfo(const LoadsetBundle& bundle);
        
        friend LoadsetBundleInfo getLoadsetBundleInfo(const std::string bundlePath);

    protected:
        std::shared_ptr<LoadsetBundle>      m_bundle;
        
        std::vector<LoadsetInfo>            m_loadsets;
        
    public:
        /*! The version of the contained schema module */
        const VersionInfo moduleVersion() const;
        
        /*! The effectivity information of this loadset bundle */
        const EffectivityInfo effectivity() const;
        
        /*! The baseline serial number (only populated for DELTA loadsets) */
        const std::string& baselineSerial() const;

        /*! The  serial number of the contained data */
        const std::string& serial() const;
        
        /*! The type of the loadset bundle */
        const LoadsetType& type() const;
        
        /*! Sum of all records contained in the loadset bundle */
        const size_t recordCount() const;
        
        /*! Schema module versions this loadset is also compatible with */
        const std::vector<VersionInfo>& compatibleVersions() const;
        
        /*! The metadata for each loadset contained in the loadset bundle */
        const std::vector<LoadsetInfo>& loadsets() const;
        
        /*! Additional product.* metadata stored in the loadset bundle */
        const std::map<std::string, std::string>& additionalData() const;
    };

    /*!
     * Read the metadata from the provided loadset bundle.
     *
     * @param bundlePath The path to the loadset bundle which either can be a
     *        folder containing the loadset data, or a ZIP archive with the
     *        loadset bundle data.
     * @return A LoadsetBundleInfo instance with the metadata of the loadset bundle
     * @throws JDSException if an error occurred accessing the loadset bundle
     */
    LoadsetBundleInfo getLoadsetBundleInfo(const std::string bundlePath);
}

#endif
