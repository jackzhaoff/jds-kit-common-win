///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSLoadsetAPI.h
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        Define the C++ JDS Data Access API
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#ifdef __cplusplus

#include <JDSKit/JDSKit.hpp>
#include <JDSKit/JDSConnection.hpp>

namespace jds
{
    enum RecordOperationType
    {
        UNKNOWN     = 0,
        
        REMOVED     = 1,
        
        STORED      = 2
    };
    
    typedef enum RecordOperationType RecordOperationType;
    
    enum PartOperationType
    {
        PART_REMOVED    = 1,
        
        PART_ADDED      = 2
    };
    
    typedef enum PartOperationType PartOperationType;
    
    /*!
     * Open a JDS store connection. Since a JDS Schema can be comprised out of multiple
     * schema modules, all modules will be opened and attached to the database connection.
     *
     * In order to allow this function to find the correct storage location for each store,
     * the provided StoreConfigurations instance is used to provide path and optional the
     * cryptographics cipher for each schema module.
     *
     * @param schema The JDS schema instance to associate with the connection.
     * @param storeConfigs The JDS StoreConfiguration instances.
     * @return The DBConnection with all stores attached.
     * @throws JDSException if the operation cannot be performed for any reason.
     */
    DBConnection openStore(const Schema& schema, const StoreConfigurations& storeConfigs);
    
    /*!
     * Create a JDS store connection with newly created data stores. Since a JDS Schema can
     * be comprised out of multiple schema modules, all modules will be created and attached
     * to the database connection.
     *
     * In order to allow this function to find the correct storage location for each store,
     * the provided StoreConfigurations instance is used to provide path and optional the
     * cryptographics cipher for each schema module.
     *
     * @param schema The JDS schema instance to associate with the connection.
     * @param storeConfigs The JDS StoreConfiguration instances.
     * @return The DBConnection with all new stores attached.
     * @throws JDSException if the operation cannot be performed for any reason.
     */
    DBConnection createStore(const Schema& schema, const StoreConfigurations& storeConfigs);
    
    /*!
     * Retrieve the type of modification that happened for the provided record identifier
     * during the last data load.
     *
     * @param connection The database connection
     * @param typeOrdinal The type ordinal of the feature to be stored in the database
     * @param identifier The primary key of the record to be stored.
     * @return A flag indicating the detected operation
     * @throws JDSException if the operation failed.
     */
    RecordOperationType getModificationForRecord(DBConnection& connection,
                                           const TypeOrdinal typeOrdinal,
                                           const unsigned long identifier);
    
    /*!
     * Retrieve the type of modification that happened for the provided record identifier
     * during the last data load.
     *
     * @param connection The database connection handle
     * @param module The particular module containing the typeOrdinal below
     * @param typeOrdinal The type ordinal of the feature to be stored in the database
     * @param identifier The primary key of the record to be stored.
     * @return A flag indicating the detected operation
     * @throws JDSException if the operation failed.
     */
    RecordOperationType getModificationForRecord(DBConnection& connection,
                                           const std::string& module,
                                           const TypeOrdinal typeOrdinal,
                                           const unsigned long identifier);
    

    typedef std::function<bool(const TypeOrdinal typeOrdinal,
                               const unsigned long identifier,
                               const RecordOperationType operation)> RecordModificationCallback;
    
    typedef std::function<bool(const std::string& part,
                               const PartOperationType operation)> PartModificationCallback;
    

    /*!
     * Retrieve the type of modification that occurred for a given feature in a
     * particular module during the last data update
     *
     * @param connection The database connection handle
     * @param module The particular module containing the typeOrdinal below
     * @param typeOrdinal The type ordinal of the feature to be stored in the database
     * @param callback A lambda expression receiving the record identification
     * @throws JDSException if the operation failed.
     */
    void getModificationForType(DBConnection& connection,
                                const std::string& module,
                                const TypeOrdinal typeOrdinal,
                                RecordModificationCallback callback);

    /*!
     * Retrieve the type of modification that occurred for a given feature in a
     * particular module during the last data update
     *
     * @param connection The database connection handle
     * @param module The particular module containing the typeOrdinal below
     * @param callback A lambda expression receiving the record identification
     * @throws JDSException if the operation failed.
     */
    void getModifiedParts(DBConnection& connection,
                          const std::string& module,
                          PartModificationCallback callback);
}

#endif
