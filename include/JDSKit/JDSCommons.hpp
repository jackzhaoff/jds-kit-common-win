///::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// @file: JDSCommons.hpp
//
// Jeppesen Sanderson, Inc. Confidential & Proprietary
//
// This work contains valuable confidential and proprietary
// information.  Disclosure, use or reproduction outside of
// Jeppesen Sanderson, Inc. is prohibited except as authorized
// in writing.  This unpublished work is protected by the laws
// of the United States and other countries.  In the event of
// publication, the following notice shall apply:
//
// Copyright 2017-2018  Jeppesen Sanderson, Inc.  All Rights Reserved
//
// @brief
//        Provide common data types, structures, and classes used by
//        all JDS APIs
//
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#ifdef __cplusplus

#include <string>
#include <map>

namespace jds
{
    template <typename T> class optional
    {
    public:
        optional() :
            m_hasValue(false)
        {};
        
        optional(const T& other) :
            m_hasValue(true),
            m_value(other)
        {}
        
        optional(const optional<T>& other) :
            m_hasValue(other.m_hasValue),
            m_value(other.m_value)
        {}
        
        optional<T>& operator =(const T other)
        {
            m_hasValue = true;
            m_value = other;
            
            return *this;
        }
        
        optional<T>& operator =(const optional<T> other)
        {
            m_hasValue = other.m_hasValue;
            m_value = other.m_value;
            
            return *this;
        }
        
        bool operator ==(const optional<T> other) const
        {
            if (m_hasValue == other.m_hasValue) {
                if (m_hasValue) {
                    return (other.m_value == m_value);
                }
                else {
                    return true;
                }
            }
            
            return false;
        }
        
        bool operator ==(const T other) const
        {
            return (m_hasValue && (other == m_value));
        }
        
        explicit operator bool() const
        {
            return m_hasValue;
        }
        
        operator T() const
        {
            return m_value;
        }
        
        inline T get() const
        {
            return m_value;
        }
        
        inline bool is_initialized() const
        {
            return m_hasValue;
        }
        
    protected:
        bool    m_hasValue;
        T       m_value;
    };
    
    /*!
     * 
     */
    struct EffectivityInfo {
        
        long            DataVersion;
        
        optional<long>  DataRevision;

        optional<long>  EffectiveFrom;
        optional<long>  EffectiveTo;
    };
    
    typedef struct EffectivityInfo EffectivityInfo;
    
    /*!
     * Provide information about a JDS store
     */
    struct StoreConfiguration
    {
        const std::string    ModuleName;
        
        const std::string    Path;
        
        const std::string    Cipher;
        
        StoreConfiguration(const std::string moduleName,
                           const std::string path,
                           const std::string cipher) :
            ModuleName(moduleName),
            Path(path),
            Cipher(cipher)
        {}
    };
    
    typedef struct StoreConfiguration StoreConfiguration;
    
    
    struct StoreConfigurations
    {
        std::map<std::string, StoreConfiguration>  m_configs;
        
        void addStoreConfig(const std::string moduleName,
                            const std::string path,
                            const std::string cipher = "") {
            
            m_configs.insert( std::pair<std::string, StoreConfiguration>(moduleName,
                                                                         StoreConfiguration(moduleName,
                                                                                            path,
                                                                                            cipher)));
        }
    };
    
    typedef struct StoreConfigurations StoreConfigurations;

    /*!
     * Describe the level of
     */
    enum ValidationScope
    {
        FAST        = 1,
        
        COMPLETE    = 3
    };
    
    
    /*!
     * Describe the status of a JDS store
     */
    enum StoreState
    {
        /*! The store file does not exist */
        JDS_STORE_WRONG_SCHEMA  = 4,
        
        /*! The store file does not exist */
        JDS_STORE_NOT_EXISTING  = 3,
        
        /*! The store is empty */
        JDS_STORE_EMPTY         = 2,
        
        /*! The store is not a JDS store */
        JDS_STORE_INVALID       = 1,
        
        /*! The store is a valid JDS store */
        JDS_STORE_VALID         = 0
    };
    
    /**
      * Return the last error message that was recorded. This will be cleared upon
      * any successive invocation of a JDS API
      */
    const char* lastErrorMessage();
    
    /**
     * Return the last error code that was recorded. This will be cleared upon
     * any successive invocation of a JDS API
     */
    const int lastErrorCode();
}

#endif
